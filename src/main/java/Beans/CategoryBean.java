/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import POJO.Category;
import POJO.TestStep;
import POJO.TestStep_Category;
import Services.CategoryService;
import Services.QuestionService;
import Services.AnswerService;
import Services.TestStep_CategoryService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Lisa
 */
public class CategoryBean implements Serializable{

    private NavigationBean navi;
    private CategoryService catS = CategoryService.getInstance();
    private QuestionService quS = QuestionService.getInstance();
    private AnswerService aService = AnswerService.getInstance();
    private List<Category> categoriesOfTestStep=new ArrayList<>();
    private List<TestStep_Category> t_cList=new ArrayList<>();
    private TestStep_CategoryService t_cService = TestStep_CategoryService.getInstance();
    private TestStepBean stepBean;

     public void home(ActionEvent event){
        navi.setTest(null);
        navi.setTestStep(null);
        navi.setCategory(null);
    }     

    public NavigationBean getNavi() {
        return navi;
    }

    public void setNavi(NavigationBean navi) {
        this.navi = navi;
    }

    public TestStepBean getStepBean() {
        return stepBean;
    }

    public void setStepBean(TestStepBean stepBean) {
        this.stepBean = stepBean;
    }

    public CategoryService getCatS() {
        return catS;
    }

    public void setCatS(CategoryService catS) {
        this.catS = catS;
    }

    public QuestionService getQuS() {
        return quS;
    }

    public void setQuS(QuestionService quS) {
        this.quS = quS;
    }

    public AnswerService getaService() {
        return aService;
    }

    public void setaService(AnswerService aService) {
        this.aService = aService;
    }
    
    public void saveNavigation(Category cat){
        navi.setCategory(cat);
    }
    public List<Category> getCategoriesOfTestStep() {
        TestStep teststep = navi.getTestStep();
        categoriesOfTestStep.clear();

        if (teststep != null) {
            t_cList.clear();

            for (TestStep_Category tc : t_cService.getList()) {
                if (tc.getTestStepID() == teststep.getId()) {
                    for (Category c : catS.getList()) {
                        if (c.getId() == tc.getCategoryID()) {
                            categoriesOfTestStep.add(c);
                        }
                    }
                }

            }           
        }
        return categoriesOfTestStep;
    }
    public List<Category> getCategoriesPerTestStep(TestStep teststep) {       
        TestStep oldStep=navi.getTestStep();
        navi.setTestStep(teststep);
        categoriesOfTestStep=getCategoriesOfTestStep();
        navi.setTestStep(oldStep);
        return categoriesOfTestStep;
    }
    public List<Category> getCategoriesOfTest() {
        List<Category> catsPerTest=new ArrayList<>();
        if(navi.getTest()!=null){
            TestStep actualStep = navi.getTestStep();
            List<TestStep> stepList =stepBean.getTestStepsOfTest();
            for(TestStep step:stepList){
                navi.setTestStep(step);
                List<Category> catsPerStep=getCategoriesPerTestStep(step);
                for(Category c: catsPerStep){
                    if(!catsPerTest.contains(c)){
                        catsPerTest.add(c);
                    }
                }
            }
            navi.setTestStep(actualStep);
        }
        return catsPerTest;
    }
}
