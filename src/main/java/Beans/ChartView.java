/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import POJO.Statistics;
import Services.StatisticsService;
import java.io.Serializable;
import java.util.ArrayList;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;

/**
 *
 * @author Lisa
 */
public class ChartView implements Serializable {

    private LineChartModel lineModel2;
    private LineChartModel lineModelAdmin;
    int jänCnt = 0;
    int febCnt = 0;
    int märCnt = 0;
    int aprCnt = 0;
    int maiCnt = 0;
    int junCnt = 0;
    int julCnt = 0;
    int augCnt = 0;
    int sepCnt = 0;
    int oktCnt = 0;
    int novCnt = 0;
    int dezCnt = 0;
    ArrayList<Integer> cntList = new ArrayList<>();
    ArrayList<Statistics> stListJän = new ArrayList<>();
    ArrayList<Statistics> stListFeb = new ArrayList<>();
    ArrayList<Statistics> stListMär = new ArrayList<>();
    ArrayList<Statistics> stListApr = new ArrayList<>();
    ArrayList<Statistics> stListMai = new ArrayList<>();
    ArrayList<Statistics> stListJun = new ArrayList<>();
    ArrayList<Statistics> stListJul = new ArrayList<>();
    ArrayList<Statistics> stListAug = new ArrayList<>();
    ArrayList<Statistics> stListSep = new ArrayList<>();
    ArrayList<Statistics> stListOkt = new ArrayList<>();
    ArrayList<Statistics> stListNov = new ArrayList<>();
    ArrayList<Statistics> stListDez = new ArrayList<>();

    private StatisticsService ss = StatisticsService.getInstance();
    private LoginBean lB;

    ArrayList<Integer> yearList = ss.getAvailableYears();
    Integer year;

    public void init() {
        createLineModel();
        createLineModelAdmin();
    }

    public LineChartModel getLineModel2() {
        return lineModel2;
    }

    public LineChartModel getLineModelAdmin() {
        return lineModelAdmin;
    }

    private void createLineModel() {

        lineModel2 = initStModel();
        lineModel2.setTitle("Login Diagramm");
        lineModel2.setLegendPosition("e");
        lineModel2.setShowPointLabels(true);
        lineModel2.getAxes().put(AxisType.X, new CategoryAxis("Monate"));
        Axis yAxis = lineModel2.getAxis(AxisType.Y);
        yAxis.setLabel("Anzahl der Logins der Feuerwehr");
        yAxis.setMin(0);
    }

    private LineChartModel initStModel() {
        LineChartModel model = new LineChartModel();

        ChartSeries logins = new ChartSeries();
        logins.setLabel("Login");

        for (int idx = 0; idx < ss.getList().size(); idx++) {
            if (ss.getList().get(idx).getFirebrigadeID() == lB.getLs().getCurrentUser().getFirebrigadeID()) {
                if (ss.getList().get(idx).getActualDate().getYear() == year) {
                    int month = ss.getList().get(idx).getActualDate().getMonthValue(); // getMonthValue starts with 1 not with 0
                    String monthStr = this.getMonthString(month);
                    logins.set(monthStr, ss.getList().get(idx).getCounter());
                }
            }
        }

        model.addSeries(logins);
        return model;
    }

    private String getMonthString(int month) {
        String monthStr = null;
        switch (month) {
            case 1:
                monthStr = "Jän";
                break;
            case 2:
                monthStr = "Feb";
                break;
            case 3:
                monthStr = "Mär";
                break;
            case 4:
                monthStr = "Apr";
                break;
            case 5:
                monthStr = "Mai";
                break;
            case 6:
                monthStr = "Jun";
                break;
            case 7:
                monthStr = "Jul";
                break;
            case 8:
                monthStr = "Aug";
                break;
            case 9:
                monthStr = "Sept";
                break;
            case 10:
                monthStr = "Okt";
                break;
            case 11:
                monthStr = "Nov";
                break;
            case 12:
                monthStr = "Dez";
                break;
        }
        return monthStr;
    }

    private void createLineModelAdmin() {
        lineModelAdmin = initStModelAdmin();
        lineModelAdmin.setTitle("Login Diagramm");
        lineModelAdmin.setLegendPosition("e");
        lineModelAdmin.setShowPointLabels(true);
        lineModelAdmin.getAxes().put(AxisType.X, new CategoryAxis("Monate"));
        Axis yAxis = lineModelAdmin.getAxis(AxisType.Y);
        yAxis.setLabel("Anzahl der Logins aller Feuerwehren");
        yAxis.setMin(0);
    }

    private LineChartModel initStModelAdmin() {
        LineChartModel model = new LineChartModel();

        ChartSeries logins = new ChartSeries();
        logins.setLabel("Logins");
        int month = 0;
        String[] monthArray = {"Jän", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sept", "Okt", "Nov", "Dez"};

        stListJän = ss.getStatisticsPerMonth(1, year);
        stListFeb = ss.getStatisticsPerMonth(2, year);
        stListMär = ss.getStatisticsPerMonth(3, year);
        stListApr = ss.getStatisticsPerMonth(4, year);
        stListMai = ss.getStatisticsPerMonth(5, year);
        stListJun = ss.getStatisticsPerMonth(6, year);
        stListJul = ss.getStatisticsPerMonth(7, year);
        stListAug = ss.getStatisticsPerMonth(8, year);
        stListSep = ss.getStatisticsPerMonth(9, year);
        stListOkt = ss.getStatisticsPerMonth(10, year);
        stListNov = ss.getStatisticsPerMonth(11, year);
        stListDez = ss.getStatisticsPerMonth(12, year);

        jänCnt = this.addCounters(stListJän);
        febCnt = this.addCounters(stListFeb);
        märCnt = this.addCounters(stListMär);
        aprCnt = this.addCounters(stListApr);
        maiCnt = this.addCounters(stListMai);
        junCnt = this.addCounters(stListJun);
        julCnt = this.addCounters(stListJul);
        augCnt = this.addCounters(stListAug);
        sepCnt = this.addCounters(stListSep);
        oktCnt = this.addCounters(stListOkt);
        novCnt = this.addCounters(stListNov);
        dezCnt = this.addCounters(stListDez);
        this.addCountersToList();

        for (int idx = 0;
                idx < cntList.size();
                idx++) {
            logins.set(monthArray[idx], cntList.get(idx));
        }

        model.addSeries(logins);
        return model;
    }

    public int addCounters(ArrayList<Statistics> list) {
        int cnt = 0;
        for (int idx = 0; idx < list.size(); idx++) {
            cnt += list.get(idx).getCounter();
        }
        return cnt;
    }

    public void addCountersToList() {
        cntList.add(jänCnt);
        cntList.add(febCnt);
        cntList.add(märCnt);
        cntList.add(aprCnt);
        cntList.add(maiCnt);
        cntList.add(junCnt);
        cntList.add(julCnt);
        cntList.add(augCnt);
        cntList.add(sepCnt);
        cntList.add(oktCnt);
        cntList.add(novCnt);
        cntList.add(dezCnt);
    }

    public LoginBean getlB() {
        return lB;
    }

    public void setlB(LoginBean lB) {
        this.lB = lB;
    }

    public ArrayList<Integer> getYearList() {
        return yearList;
    }

    public void setYearList(ArrayList<Integer> yearList) {
        this.yearList = yearList;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
   
}
