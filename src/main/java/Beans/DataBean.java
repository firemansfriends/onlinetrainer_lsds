/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import POJO.Category;
import POJO.Test;
import POJO.TestStep;
import POJO.TestStep_Category;
import Services.CategoryService;
import Services.TestService;
import Services.TestStepService;
import Services.TestStep_CategoryService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Denise
 */

public class DataBean implements Serializable {

    private String newTestname = "";
    private TestService testService = TestService.getInstance();
    private String errMessage = "";
    private TestStepService teststepService = TestStepService.getInstance();
    private Test currTest = new Test(0, "",false);
    private TestStep currStep = new TestStep("", 0);
    private List<TestStep> stepList = new ArrayList<>();
    private String newStepname = "";
    private NavigationBean navi;
    private CategoryService catService = CategoryService.getInstance();
    private String[] navList = {"", ""};
    private String newCatName = "";
    private Category currCat = new Category("");
    private TestStep_CategoryService stepCatService = TestStep_CategoryService.getInstance();
    private Category currCatForStep = new Category("");

    public Category getCurrCatForStep() {
        return currCatForStep;
    }

    public NavigationBean getNavi() {
        return navi;
    }

    public void setNavi(NavigationBean navi) {
        this.navi = navi;
    }

    public void setCurrCatForStep(Category currCatForStep) {
        this.currCatForStep = currCatForStep;
    }

    public String getNewCatName() {
        return newCatName;
    }

    public void setNewCatName(String newCatName) {
        this.newCatName = newCatName;
    }

    public CategoryService getCatService() {
        return catService;
    }

    public void setCatService(CategoryService catService) {
        this.catService = catService;
    }

    public String getNewStepname() {
        return newStepname;
    }

    public void setNewStepname(String newStepname) {
        this.newStepname = newStepname;
    }

    public String getNewTestname() {
        return newTestname;
    }

    public void setNewTestname(String newTestname) {
        this.newTestname = newTestname;
    }

    public void changeTestName() {
        if (currTest.getId() != 0) {
            if (!newTestname.isEmpty()) {
                currTest.setName(newTestname);
                testService.updateObject(currTest);
                errMessage = "";
                newTestname = "";
                currTest = new Test(0, "",false);
            } else {
                errMessage = "Testnamen eingeben!";
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
            }
        } else {
            errMessage = "Test auswählen!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
        }
    }

    public void changeTeststepName() {
        if (currStep.getId() != 0) {
            if (!newStepname.isEmpty()) {
                currStep.setName(newStepname);
                teststepService.updateObject(currStep);
                errMessage = "";
                newStepname = "";
                currStep = new TestStep("", 0);
            } else {
                errMessage = "Teststufennamen eingeben!";
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
            }
        } else {
            errMessage = "Teststufe auswählen!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
        }
    }

    public void addTest() {
        if (!newTestname.isEmpty()) {
            testService.insertObject(new Test(newTestname,false));
            errMessage = "";
        } else {
            errMessage = "Testname eingeben!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
        }
        newTestname = "";
    }

    public void addTestStep() {
        if (!newStepname.isEmpty()) {
            if (navi.getTest() == null) {
                errMessage = "Test auswählen!";
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
            } else {
                teststepService.insertObject(new TestStep(newStepname, navi.getTest().getId()));
                errMessage = "";
            }
        } else {
            errMessage = "Teststufenname eingeben!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
        }
        newStepname = "";
    }

    public void deleteTest(Test t) {
        if (testService.deleteObject(t)) {
            errMessage = "";
        } else {
            errMessage = "Dieser Test kann nicht gelöscht werden, es müssen zuerst alle zugehörigen Stufen, Kategorien und Fragen gelöscht werden!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
        }
    }

    public void deleteTeststep(TestStep t) {
        if(teststepService.deleteObject(t)){
            errMessage = "";
        }else{
            errMessage = "Dieser Stufe kann nicht gelöscht werden, es müssen zuerst alle zugehörigen Kategorien und Fragen gelöscht werden!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
        }
    }

    public TestStepService getTeststepService() {
        return teststepService;
    }

    public void setTeststepService(TestStepService teststepService) {
        this.teststepService = teststepService;
    }

    public void setCurrentTest(Test t) {
        this.currTest = t;
        this.newTestname = currTest.getName();
        errMessage = "";
    }

    public void setCurrentTeststep(TestStep t) {
        this.currStep = t;
        this.newStepname = currStep.getName();
        errMessage = "";
    }

    public void showTeststeps(Test t) {
        currCat = new Category("");
        currStep = new TestStep("", 0);
        errMessage = "";
        currTest = t;
        navi.setTest(t);
        navi.setTestStep(null);
        navList[0] = t.getName();
        navList[1] = "";
    }

    public void showCategories(TestStep t) {
        currCat = new Category("");
        errMessage = "";
        currStep = t;
        navi.setTestStep(t);
        navList[0] = navi.getTest().getName();
        navList[1] = t.getName();
    }

    public Test getCurrTest() {
        return currTest;
    }

    public List<TestStep> getStepList() {
        return stepList;
    }

    public void setStepList(List<TestStep> stepList) {
        this.stepList = stepList;
    }

    public TestService getTestService() {
        return testService;
    }

    public void setTestService(TestService testService) {
        this.testService = testService;
    }

    public String navString() {
        if (navList[0].isEmpty() && navList[1].isEmpty()) {
            return "";
        }
        return navList[0] + " > " + navList[1];
    }

    public void setCurrentCategory(Category c) {
        this.currCat = c;
        this.newCatName = currCat.getName();
        errMessage = "";
    }

    public void deleteCategory(Category c) {
        TestStep_Category tsC = null;
        for (TestStep_Category tc : stepCatService.getList()) {
            if (tc.getCategoryID() == c.getId() && tc.getTestStepID() == navi.getTestStep().getId()) {
                tsC = tc;
            }
        }
        if (tsC != null) {
            stepCatService.deleteObject(tsC);
        }
        errMessage = "";
    }

    public void addCat() {
        if (!newCatName.isEmpty()) {
            if (navi.getTestStep() == null) {
                errMessage = "Teststufe auswählen!";
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
            } else {
                Category c = new Category(newCatName);
                catService.insertObject(c);
                stepCatService.insertObject(new TestStep_Category(navi.getTestStep().getId(), c.getId()));
                errMessage = "";
            }
        } else {
            errMessage = "Kategorienamen eingeben!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
        }
        newCatName = "";
    }

    public void changeCatName() {
        if (currCat.getId() != 0) {
            if (!newCatName.isEmpty()) {
                currCat.setName(newCatName);
                catService.updateObject(currCat);
                errMessage = "";
                newCatName = "";
                currCat = new Category("");
            } else {
                errMessage = "Kategorienamen eingeben!";
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
            }
        } else {
            errMessage = "Kategorie auswählen!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
        }
    }

    public void addCatToStep() {
        if (currCatForStep != null && currCatForStep.getId() != 0) {
            if (navi.getTestStep() == null) {
                errMessage = "Teststufe auswählen!";
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
            } else {
                boolean exists = false;
                for (TestStep_Category tc : stepCatService.getList()) {
                    if (tc.getCategoryID() == currCatForStep.getId() && tc.getTestStepID() == navi.getTestStep().getId()) {
                        exists = true;
                    }
                }
                if (!exists) {
                    stepCatService.insertObject(new TestStep_Category(navi.getTestStep().getId(), currCatForStep.getId()));
                    errMessage = "";
                }else{
                    errMessage = "Kategorie bereits in dieser Teststufe vorhanden!";
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
                }
            }
        } else {
            errMessage = "Kategorienamen aus Listbox auswählen!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, errMessage, ""));
        }
    }
    public void changeActive(Test t){
        testService.updateObject(t);
    }
}
