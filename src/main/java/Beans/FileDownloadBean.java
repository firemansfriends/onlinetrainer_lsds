/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Beans;

import java.io.InputStream;
import java.io.Serializable;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Denise
 */
public class FileDownloadBean implements Serializable{

    private StreamedContent file;
    private NavigationBean navi;

    public NavigationBean getNavi() {
        return navi;
    }

    public void setNavi(NavigationBean navi) {
        this.navi = navi;
    }
 
    public StreamedContent getHelpFile() {
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/help/Helpbook.pdf");
        file = new DefaultStreamedContent(stream, "application/pdf", "Hilfe.pdf");
        return file;
    }
    public StreamedContent getAdminHelp() {
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/help/Adminhelp.pdf");
        file = new DefaultStreamedContent(stream, "application/pdf", "Administratorhandbuch.pdf");
        return file;
    }
    
    public StreamedContent getSuperuserHelp() {
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/help/Superuserhelp.pdf");
        file = new DefaultStreamedContent(stream, "application/pdf", "Superuserhandbuch.pdf");
        return file;
    }
    
    public StreamedContent getInstructionFile() {
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/help/ImportAnleitung.pdf");
        file = new DefaultStreamedContent(stream, "application/pdf", "ImportAnleitung.pdf");
        return file;
    }
    public StreamedContent getLearningDoc() {
        String fileName=navi.getTestStep().getName();
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/Lernunterlagen/"+fileName+".pdf");
        file = new DefaultStreamedContent(stream, "application/pdf", fileName+".pdf");
        return file;
    }
    public StreamedContent getCSVTemplate() {
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/templates/FragenImportieren.csv");
        file = new DefaultStreamedContent(stream, "text/csv", "FragenImportieren.csv");
        return file;
    }
}
