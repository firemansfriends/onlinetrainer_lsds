/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Beans;

import POJO.Firebrigade;
import Services.FirebrigadeService;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Denise
 */
public class FirebrigadeBean implements Serializable{
    private LoginBean lB;
    private Firebrigade firebrigade;
    private FirebrigadeService firebrigadeService = FirebrigadeService.getInstance();

    @PostConstruct
    public void post(){
        firebrigade=lB.getLs().getCurrentfirebrigade();
    }

    public LoginBean getlB() {
        return lB;
    }

    public void setlB(LoginBean lB) {
        this.lB = lB;
    }
 
    public Firebrigade getFirebrigade() {
        return firebrigade;
    }

    public void setFirebrigade(Firebrigade firebrigade) {
        this.firebrigade = firebrigade;
    }

    public FirebrigadeService getFirebrigadeService() {
        return firebrigadeService;
    }

    public void setFirebrigadeService(FirebrigadeService firebrigadeService) {
        this.firebrigadeService = firebrigadeService;
    }
    public void saveFirebrigade(){
        try{
            Integer.parseInt(firebrigade.getFoundingYear());
            firebrigadeService.updateObject(firebrigade);           
        }catch(NumberFormatException e){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Fehler bei Gründungsjahr!", ""));
        }
    }
    
}
