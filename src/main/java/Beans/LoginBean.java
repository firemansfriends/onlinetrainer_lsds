/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Email.SendingEmails;
import POJO.Firebrigade;
import POJO.Statistics;
import Services.FirebrigadeService;
import Services.LoginService;
import Services.StatisticsService;
import Services.UserService;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Lisa
 */
public class LoginBean implements Serializable {

    private boolean isLoggedIn = false;
    private ArrayList<String> firebrigades = new ArrayList<>();
    private FirebrigadeService fs = FirebrigadeService.getInstance();
    private List<Firebrigade> firebrigadeListPerDistrict;
    private UserService us = UserService.getInstance();
    private SendingEmails sendingEmail = new SendingEmails();
    private StatisticsService ss = StatisticsService.getInstance();
    private LoginService ls = new LoginService();

    public String loginAdmin() {
        String loginsuccess = null;
        int numberOfRuns = 0;
        for (int idx = 0; idx < us.getList().size(); idx++) {
            if (ls.getUsername().equals(us.getList().get(idx).getUsername())) {
                if (ls.getPassword().hashCode() == us.getList().get(idx).getPassword()) {
                    if (us.getList().get(idx).getUserrightID() == 1
                            || us.getList().get(idx).getUserrightID() == 2) {
                        ls.setNameOfUser(ls.getUsername());
                        ls.setId(us.getList().get(idx).getId());
                        ls.setCurrentUser(us.readObject(ls.getId()));
                        ls.setCurrentfirebrigade(fs.readObject(ls.getCurrentUser().getFirebrigadeID()));
                        ls.setUserrightID(us.getList().get(idx).getUserrightID());
                        this.isLoggedIn = true;
                        FacesContext facesContext = FacesContext.getCurrentInstance();
                        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
                        session.setAttribute("isLoggedIn", isLoggedIn);
                        if (us.getList().get(idx).getEmail() == null || us.getList().get(idx).getEmail().equals("")) {
                            loginsuccess = "/enterMail.xhtml";
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                                    "Bitte geben Sie zusätzlich Ihre Mail Adresse an. Diese wird benötigt, um das Passwort nach Bedarf zurückzusetzen. "
                                    + "Diese Eingabe erfolgt einmalig."));
                        } else {
                            loginsuccess = "/home.xhtml";
                            this.ls.setEmail(ls.getCurrentUser().getEmail());

                            if (us.getList().get(idx).getUserrightID() == 1) {
                                session.setAttribute("userrightID", 1);
                            } else {
                                if (us.getList().get(idx).getUserrightID() == 2) {
                                    session.setAttribute("userrightID", 2);
                                }
                            }
                            break;
                        }
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login Error", "Keine Berechtigung!"));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login Error", "Falscher Benutzername oder Passwort!"));
                }
            } else {
                numberOfRuns++;
                if (numberOfRuns == us.getList().size()) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login Error", "Falscher Benutzername oder Passwort!"));
                }
            }
        }
        return loginsuccess;
    }

    public String login() {
        String loginsuccess = null;
        ArrayList<Integer> idsToFirebrigade = new ArrayList<>();
        idsToFirebrigade = ss.getIDsToFirebrigadeID(ls.getFirebrigade().getId());
        if (idsToFirebrigade.size() > 0) {
            ls.setCurrentStatistics(ss.readObject(idsToFirebrigade.get(idsToFirebrigade.size() - 1)));
            if (!checkDate(ls.getCurrentStatistics().getActualDate())) {
                ls.getCurrentStatistics().setActualDate(LocalDate.now());
                ls.getCurrentStatistics().setCounter(0);
                ss.insertObject(new Statistics(ls.getCurrentStatistics().getFirebrigadeID(), ls.getCurrentStatistics().getCounter(), ls.getCurrentStatistics().getActualDate()));
                idsToFirebrigade = ss.getIDsToFirebrigadeID(ls.getFirebrigade().getId());
                ls.setCurrentStatistics(ss.readObject(idsToFirebrigade.get(idsToFirebrigade.size() - 1)));
            }
        }
        if (ls.getName() != null && ls.getFirebrigade() != null && (!ls.getName().isEmpty())) {
            loginsuccess = "/home.xhtml";
            ls.setNameOfUser(ls.getName());
            this.isLoggedIn = true;
            FacesContext facesContext = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
            session.setAttribute("isLoggedIn", isLoggedIn);
            session.setAttribute("userrightID", 3);
            if (ls.getCurrentStatistics() == null) {
                ls.setCurrentStatistics(new Statistics(ls.getFirebrigade().getId(), 0, LocalDate.now()));
            } else {
                ls.getCurrentStatistics().setCounter(ls.getCurrentStatistics().getCounter() + 1);
            }
            ss.updateObject(ls.getCurrentStatistics());
        }
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Bitte alle Daten angeben!");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return loginsuccess;
    }

    public boolean checkDate(LocalDate d) { //checks if a new statistics row has to be added in the database
        // a new Statistics object should be created when there is a new month or a new year
        if (d.getMonthValue() == LocalDate.now().getMonthValue()
                && d.getYear() == LocalDate.now().getYear()) {
            return true;
        } else {
            return false;
        }
    }

    public String showInfo() {
        String action = "/adminlogin.xhtml";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                "Nur Benutzer mit Administratoren- bzw. Superuserrechten können sich in diesem Bereich anmelden!"));
        return action;
    }

    public String saveMail() {
        String ret = null;
        ls.getCurrentUser().setEmail(ls.getEmail());
        us.updateObject(ls.getCurrentUser());
        ret = "/home.xhtml";
        return ret;
    }

    public void savePassword() {
        if (ls.getOldPassword() != null || ls.getNewPassword() != null || ls.getNewPasswordConfirm() != null) {
            if (ls.getOldPassword().hashCode() == ls.getCurrentUser().getPassword()) {
                if (ls.getNewPassword().equals(ls.getNewPasswordConfirm())) {
                    ls.getCurrentUser().setPassword(ls.getNewPassword().hashCode());
                    us.updateObject(ls.getCurrentUser());
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                            "Neues Passwort stimmt nicht mit der Bestätigung überein!"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                        "Altes Passwort ist falsch!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                    "Bitte alle Daten eingeben!"));
        }
        ls.setOldPassword(null);
        ls.setNewPassword(null);
        ls.setNewPasswordConfirm(null);
    }

    public void save() {
        if (this.ls.isIsMailChanged()) {
            if (ls.getEmail() == null || ls.getEmail().equals("") || ls.getEmail().equals(" ")) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                        "Es wurde keine E-Mail Adresse eingegeben!"));
            } else {
                ls.getCurrentUser().setEmail(ls.getEmail());
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                        "Daten wurden geändert."));
                us.updateObject(ls.getCurrentUser());
            }
        }
        this.ls.setIsMailChanged(false);
    }

    public void readMail() {
        for (int idx = 0; idx < us.getList().size(); idx++) {
            if (ls.getUsername().equals(us.getList().get(idx).getUsername())) {
                ls.setId(us.getList().get(idx).getId());
                ls.setCurrentUser(us.readObject(ls.getId()));
                break;
            }
        }
    }

    public void sendMail() {
        if (ls.getUsername() != null && !(ls.getUsername().equals(""))) {
            readMail();
            if (ls.getCurrentUser().getEmail() != null && !(ls.getCurrentUser().getEmail().equals(""))) {
                sendingEmail.sendMail(ls.getCurrentUser().getEmail());
                ls.getCurrentUser().setPassword(sendingEmail.getGeneratedPassword().hashCode());
                us.updateObject(ls.getCurrentUser());
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                    "Keine E-Mail Adresse vorhanden"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                    "Benutzername eingeben!"));
        }
    }

    public ArrayList<String> getFirebrigades() {
        this.firebrigades.clear();
        for (int idx = 0; idx < fs.getList().size(); idx++) {
            this.firebrigades.add(fs.getList().get(idx).getLocation());
        }
        return this.firebrigades;
    }

    public void setFirebrigades(ArrayList<String> firebrigades) {
        this.firebrigades = firebrigades;
    }

    public List<Firebrigade> getFirebrigadeListPerDistrict() {
        return firebrigadeListPerDistrict;
    }

    public void setFirebrigadeListPerDistrict(List<Firebrigade> firebrigadeListPerDistrict) {
        this.firebrigadeListPerDistrict = firebrigadeListPerDistrict;
    }

    public FirebrigadeService getFs() {
        return fs;
    }

    public void setFs(FirebrigadeService fs) {
        this.fs = fs;
    }

    public boolean isIsLoggedIn() {
        return isLoggedIn;
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public LoginService getLs() {
        return ls;
    }

    public void setLs(LoginService ls) {
        this.ls = ls;
    }

}
