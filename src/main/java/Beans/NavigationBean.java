/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import POJO.Category;
import POJO.Question;
import POJO.Test;
import POJO.TestStep;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Lisa
 */
public class NavigationBean implements Serializable {

    public LoginBean lB;
    private Test test;
    private TestStep testStep;
    private Category category;
    private Question question;
    private ArrayList<String> navigationList = new ArrayList<>();

    public LoginBean getlB() {
        return lB;
    }

    public void setlB(LoginBean lB) {
        this.lB = lB;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public TestStep getTestStep() {
        return testStep;
    }

    public void setTestStep(TestStep testStep) {
        this.testStep = testStep;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public void addNavigationComponent(String str) {
        this.navigationList.add(str);
    }

    public void saveTestNavigation() {
        addNavigationComponent("Test auswählen");
    }

    public ArrayList<String> getNavigationList() {
        return this.navigationList;
    }
    public void removeNavigationComponent(int idx){
        this.navigationList.remove(idx);
    }
}
