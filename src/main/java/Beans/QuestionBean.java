/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Cookie.CookieHelper;
import POJO.Answer;
import POJO.Category;
import POJO.Question;
import POJO.TestStep;
import POJO.TestStep_Question;
import Services.AnswerService;
import Services.FirebrigadeService;
import Services.QuestionService;
import Services.TestStep_QuestionService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import jersey.repackaged.com.google.common.base.Objects;
import jersey.repackaged.com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

public class QuestionBean implements Serializable{

    private NavigationBean navi;
    private int questionCnt = 0;
    private QuestionService questionService = QuestionService.getInstance();
    private Category category;
    private Question question;
    private AnswerService answerService = AnswerService.getInstance();
    private boolean isMultCh = false;
    private boolean isText = false;
    private boolean isNumbering = false;
    private boolean isPicSelection = false;
    private boolean isComparison = false;
    private boolean isVideo = false;
    private List<String> selectedAnswers = new ArrayList<>();
    private List<Answer> answerList;
    private String action = "/questions.xhtml";
    private int questionsPerCat = 0;
    private Map<Integer, String> inputMap = new HashMap<>();
    private String hint = "";
    private int trueAnswers = 0;
    private int cnt = 1;
    private Map<Integer, Boolean> boolMap = new HashMap<>();
    private String correct = "";
    private Map<Integer, Integer> numMap = new HashMap<>();
    private Map<Integer, Answer> answerCache = Maps.newHashMap();
    private boolean isSolutionRequired = false;
    private boolean rightAnswersShouldBeShown = false;
    private int columnCnt = 0;
    private String username = "";
    private CookieHelper cookieHelper = new CookieHelper();
    private int kindOfTest = 5;//1=Fragen üben, 2=Falsch beatwortete Fragen üben
    private List<Question> qList = new ArrayList<>();
    private LoginBean loginBean;
    private FirebrigadeService fbService = FirebrigadeService.getInstance();
    private final String COMMANDER = "commander", FOUNDINGYEAR = "foundingYear", YOUTHCOMMANDER = "youthCommander", RESCUEEQUIPMENT = "rescueEquipment", SECTION = "section";
    private String categoryNameWithArrow;
    private List<Question> questionsOfCategoryPerTestStep=new ArrayList<>();
    private List<TestStep_Question> t_qList=new ArrayList<>();
    private List<Question> questionsPerTestStep=new ArrayList<>();
    private TestStep_QuestionService t_qService=TestStep_QuestionService.getInstance();

    @PostConstruct
    public void init(){
        category = navi.getCategory();
    }
    public NavigationBean getNavi() {
        return navi;
    }

    public void setNavi(NavigationBean navi) {
        this.navi = navi;
    }
    
    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Category getCategory() {
        return category = navi.getCategory();
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public QuestionService getQuestionService() {
        return questionService;
    }

    public void setQuestionService(QuestionService questionService) {
        this.questionService = questionService;
    }

    public boolean isRightAnswersShouldBeShown() {
        return rightAnswersShouldBeShown;
    }

    public void setRightAnswersShouldBeShown(boolean rightAnswersShouldBeShown) {
        this.rightAnswersShouldBeShown = rightAnswersShouldBeShown;
    }

    public Question getQuestion() {
        int quType;
        if (this.kindOfTest == 1) {
            questionsPerCat = getQuestionsOfCategoryPerTestStep().size();
            question = getQuestionsOfCategoryPerTestStep().get(questionCnt);

        }
        if (kindOfTest == 2) {
            question = qList.get(questionCnt);

        }

        quType = question.getQuestionTypeID();
        isMultCh = false;
        isText = false;
        isComparison = false;
        isNumbering = false;
        isPicSelection = false;
        isVideo = false;
        isSolutionRequired = false;
        rightAnswersShouldBeShown = true;
        columnCnt = 0;
        if (question.getId() == 19) {
            columnCnt = 5;
        }
        if (quType == 2) {
            isMultCh = true;
        }
        if (quType == 1) {
            this.isText = true;
        }
        if (quType == 3) {
            this.isComparison = true;
            this.rightAnswersShouldBeShown=false;
            if(cnt==2){
                this.isSolutionRequired=true;
            }
        }
        if (quType == 4) {
            this.isNumbering = true;
        }
        if (quType == 5) {
            this.isPicSelection = true;
        }
        if (quType == 6) {
            this.isVideo = true;
        }

        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public List<Answer> getAnswers() {
        answerList = answerService.getAnswersPerQuestion(question.getId());
        if (answerList.size() > 0) {
            if (answerList.get(0).getFbNR() != 0) {
                columnCnt = 1;
            }
        }
        return answerList;
    }

    public Map<Integer, String> getInputMap() {
        return inputMap;
    }

    public boolean isIsMultCh() {
        return isMultCh;
    }

    public void setIsMultCh(boolean isMultCh) {
        this.isMultCh = isMultCh;
    }

    public boolean isIsVideo() {
        return isVideo;
    }

    public void setIsVideo(boolean isVideo) {
        this.isVideo = isVideo;
    }

    public boolean isIsText() {
        return isText;
    }

    public void setIsText(boolean isText) {
        this.isText = isText;
    }

    public boolean isIsComparison() {
        return isComparison;
    }

    public void setIsComparison(boolean isComparison) {
        this.isComparison = isComparison;
    }

    public void saveNavigation() {
        navi.addNavigationComponent("Kategorie auswählen");
    }

    public boolean isIsNumbering() {
        return isNumbering;
    }

    public void setIsNumbering(boolean isNumbering) {
        this.isNumbering = isNumbering;
    }

    public boolean isIsSolutionRequired() {
        return isSolutionRequired;
    }

    public void setIsSolutionRequired(boolean isSolutionRequired) {
        this.isSolutionRequired = isSolutionRequired;
    }

    public void next(ActionEvent e) {
        action = "/questions.xhtml";
        if (cnt == 1) {
            boolean isCorrect = true;
            if (questionCnt < questionsPerCat) {
                if (isMultCh) {
                    boolMap.remove("get");
                    for (Answer a : answerList) {
                        if (a.getCorrect()) {
                            a.setBackcolor("background:#04B404;");
                            a.setTextcolor("color:white;");
                        } else {
                            a.setBackcolor("background:#C60606;");
                            a.setTextcolor("color:white;");
                        }
                        if (!a.getCorrect() == boolMap.get(a.getId())) {
                            isCorrect = false;
                        }
                    }
                }
                if (isText) {
                    boolean fbDependent = false;
                    int fbNumUser = loginBean.getLs().getFirebrigade().getFirebrigadenumber();
                    int fbNumAns = 0;
                    inputMap.remove("get");
                    List<String> ansInputTextList = new ArrayList<>(inputMap.values());
                    for (Answer a : answerList) {
                        selectedAnswers.add(a.getText()); //Liste der richtigen Antworten aus der Datenbank in Form von List<String>
                        if ((fbNumAns = a.getFbNR()) != 0) {
                            fbDependent = true;
                            break;
                        }
                    }
                    if (fbDependent) {
                        String corrAnswer = "";
                        if (fbNumAns == 11) {
                            String ansText = answerList.get(0).getText();
                            if (ansText.equals(this.COMMANDER)) {
                                corrAnswer = loginBean.getLs().getFirebrigade().getCommander();
                            }
                            if (ansText.equals(this.YOUTHCOMMANDER)) {
                                corrAnswer = loginBean.getLs().getFirebrigade().getYouthCommander();
                            }
                            if (ansText.equals(this.FOUNDINGYEAR)) {
                                corrAnswer = loginBean.getLs().getFirebrigade().getFoundingYear();
                            }
                            if (ansText.equals(this.RESCUEEQUIPMENT)) {
                                corrAnswer = loginBean.getLs().getFirebrigade().getYouthCommander();
                            }
                            if (ansText.equals(this.SECTION)) {
                                String num = Integer.toString(loginBean.getLs().getFirebrigade().getFirebrigadenumber());
                                corrAnswer = "" + num.charAt(1) + num.charAt(2);
                            }

                        } else {
                            String userFB = Integer.toString(loginBean.getLs().getFirebrigade().getFirebrigadenumber());
                            String num = Integer.toString(answerList.get(0).getFbNR());
                            if (num.endsWith("0000")) {//bezirksspezifische Frage
                                for (Answer a : answerList) {
                                    if (Integer.toString(a.getFbNR()).startsWith("" + userFB.charAt(0))) {
                                        corrAnswer = a.getText();
                                    }
                                }
                            } else {//abschnittsspezifische Frage
                                for (Answer a : answerList) {
                                    if (Integer.toString(a.getFbNR()).startsWith("" + userFB.charAt(0) + userFB.charAt(1) + userFB.charAt(2))) {
                                        corrAnswer = a.getText();
                                        break;
                                    }
                                }
                            }
                        }
                        if (corrAnswer.equals(ansInputTextList.get(0))) {
                            answerList.get(0).setTextcolor("color:green");
                        } else {
                            answerList.get(0).setTextcolor("color:#C60606");
                            isCorrect = false;
                        }
                    } else {
                        for (Answer a : answerList) {
                            answerCache.put(Objects.hashCode(a.getText()), a);
                        }

                        for (int idx = 0; idx < ansInputTextList.size(); idx++) {
                            String answer = ansInputTextList.get(idx);
                            if (answer.length() > 0) {
                                char firstLetter = answer.charAt(0);
                                if (firstLetter > 96 && firstLetter < 123) {
                                    firstLetter = (char) (firstLetter - 32);
                                    char[] aString = answer.toCharArray();
                                    aString[0] = firstLetter;
                                    answer = new String(aString);
                                }
                            }
                            Answer a = answerCache.get(Objects.hashCode(answer));
                            if (a == null) {
                                a = findClosetMatch(answer);
                            }
                            if (a != null) {
                                findDuplicates:
                                {
                                    for (int idx2 = 0; idx2 < ansInputTextList.size(); idx2++) {
                                        if (idx2 != idx) {
                                            Answer ans = answerCache.get(Objects.hashCode(ansInputTextList.get(idx2)));
                                            if (a.getText().equals(ansInputTextList.get(idx2))) {
                                                answerList.get(idx).setTextcolor("color:#08088A");
                                                isCorrect = false;
                                                break findDuplicates;
                                            } else {
                                                if (ans == null) {
                                                    ans = findClosetMatch(ansInputTextList.get(idx2));
                                                    if (ans != null) {
                                                        if (a.getText().equals(ans.getText())) {
                                                            answerList.get(idx).setTextcolor("color:#08088A");
                                                            isCorrect = false;
                                                            break findDuplicates;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    answerList.get(idx).setTextcolor("color:green");
                                }
                            } else {
                                answerList.get(idx).setTextcolor("color:#C60606");
                                isCorrect = false;
                            }
                        }

                    }
                }
                if (isNumbering) {

                    numMap.remove("get");
                    for (Answer a : answerList) {
                        if (numMap.get(a.getId()) != a.getNumbering()) {
                            isCorrect = false;
                            a.setTextcolor("color:#C60606");
                        } else {
                            a.setTextcolor("color:green");
                        }
                    }

                }

                if (isComparison) {
                    this.rightAnswersShouldBeShown = false;
                    isCorrect = true;
                }

                if (isPicSelection) {

                    boolMap.remove("get");
                    for (Answer a : answerList) {
                        if (a.getCorrect()) {
                            a.setPicSelectionText("Richtig");
                            a.setPicSelectionColor("color:green");
                        } else {
                            a.setPicSelectionText("Falsch");
                            a.setPicSelectionColor("color:#C60606");
                        }
                        if (!a.getCorrect() == boolMap.get(a.getId())) {
                            isCorrect = false;
                        }
                    }
                }

                if (isCorrect) {
                    trueAnswers++;
                    correct = "Richtig!";
                    removeWrongQuestion();
                } else {
                    correct = "Falsch!";
                    setWrongQuestion();
                }

            }
        }

        if (cnt == 2) {
            questionCnt++;
            correct = "";

            if (questionCnt == questionsPerCat) {
                action = "/quizResult.xhtml";
                questionCnt = 0;
            }
            selectedAnswers.clear();
            inputMap.clear();
            boolMap.clear();
            numMap.clear();
            cnt--;
            resetColors();
        } else {
            cnt++;
        }
    }

    //wird nur für den Bildvergleich verwendet
    public void showSolution(ActionEvent event) {
        this.isSolutionRequired = true;
    }

    public List<String> getSelectedAnswers() {
        return selectedAnswers;
    }

    public void setSelectedAnswers(List<String> selectedAnswers) {
        this.selectedAnswers = selectedAnswers;
    }

    public String action() {
        return action;

    }

    public void valueChanged(ValueChangeEvent e) {
        UIComponent comp = e.getComponent();
        Answer a = (Answer) comp.getAttributes().get("answer");
        inputMap.put(a.getId(), (String) e.getNewValue());
    }

    public void valueChangedMult(ValueChangeEvent event) {
        UIComponent comp = event.getComponent();
        Answer a = (Answer) comp.getAttributes().get("ans");
        boolMap.put(a.getId(), (boolean) event.getNewValue());
    }

    public String getHint() {
        if (question.getHint() == null || question.getHint().isEmpty()) {
            return "";
        }
        return "Eingabehinweis: " + question.getHint();
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public int getTrueAnswers() {
        return trueAnswers;
    }

    public void setTrueAnswers(int trueAnswers) {
        this.trueAnswers = trueAnswers;
    }

    public int getQuestionsPerCat() {
        return questionsPerCat;
    }

    public void setQuestionsPerCat(int questionsPerCat) {
        this.questionsPerCat = questionsPerCat;
    }

    public Map<Integer, Boolean> getBoolMap() {
        return boolMap;
    }

    public void setBoolMap(Map<Integer, Boolean> boolMap) {
        this.boolMap = boolMap;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public boolean isIsPicSelection() {
        return isPicSelection;
    }

    public void setIsPicSelection(boolean isPicSelection) {
        this.isPicSelection = isPicSelection;
    }

    public Map<Integer, Integer> getNumMap() {
        return numMap;
    }

    public void setNumMap(Map<Integer, Integer> numMap) {
        this.numMap = numMap;
    }

    public void valueChangedNumber(ValueChangeEvent e) {
        UIComponent comp = e.getComponent();
        Answer a = (Answer) comp.getAttributes().get("answer");
        numMap.put(a.getId(), (int) e.getNewValue());
    }

    private Answer findClosetMatch(String name) {
        if (name.contains("1") || name.contains("2") || name.contains("3")
                || name.contains("4") || name.contains("5") || name.contains("6")
                || name.contains("7") || name.contains("8") || name.contains("9") || name.contains("10")) {
            if (selectedAnswers.contains(name)) {
                return new Answer();
            } else {
                return null;
            }
        } else {
            int min = 5;
            int testVal = 0;
            Answer matchedAnswer = null;
            for (Answer a : answerCache.values()) {
                testVal = StringUtils.getLevenshteinDistance(name, a.getText());
                if (testVal < min) {
                    min = testVal;
                    matchedAnswer = a;
                }
            }
            return matchedAnswer;
        }
    }

    public int getQuestionCnt() {
        return questionCnt;
    }

    public void setQuestionCnt(int questionCnt) {
        this.questionCnt = questionCnt;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public void home() {
        if (navi != null) {
            this.navi.setTest(null);
            this.navi.setTestStep(null);
            this.navi.setCategory(null);
            this.navi.setQuestion(null);
            navi.getNavigationList().clear();
        }
        resetQuestionForNavigation();
    }

    public void resetQuestionForNavigation() {
        if (selectedAnswers != null) {
            selectedAnswers.clear();
        }
        if (inputMap != null) {
            inputMap.clear();
        }
        if (boolMap != null) {
            boolMap.clear();
        }
        if (numMap != null) {
            numMap.clear();
        }
        trueAnswers = 0;
        correct = "";
        action = "";
        questionCnt = 0;
        cnt = 1;
        qList.clear();
        resetColors();
    }

    private void resetColors() {
        if (answerList != null) {
            for (Answer a : answerList) {
                a.setBackcolor("background:white");
                a.setTextcolor("color:black");
                a.setPicSelectionText("");
                a.setPicSelectionColor("color:black");
            }
        }
    }

    public int getColumnCnt() {
        return columnCnt;
    }

    public void setColumnCnt(int columnCnt) {
        this.columnCnt = columnCnt;
    }

    public void setPracticeKind(ActionEvent e) {
        this.kindOfTest = 1;
        navi.addNavigationComponent("Kategorie auswählen");
    }

    public String setPracticeWrongKind() {
        this.kindOfTest = 2;
        Cookie c = cookieHelper.getCookie(Integer.toString(navi.getTestStep().getId()));
        if (c != null) {
            if (!c.getValue().isEmpty()) {
                String questionIds = c.getValue();
                String[] id = questionIds.split("a");

                for (int idx = 1; idx < id.length; idx++) {
                    qList.add(questionService.readObject(Integer.parseInt(id[idx])));
                }
                questionsPerCat = qList.size();
                return "/questions.xhtml";
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                    "Keine falsch beantwortete Fragen in dieser Stufe!"));
            return "/testStep.xhtml";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                    "Keine falsch beantworteten Fragen in dieser Stufe!"));
            return "/testStep.xhtml";
        }
    }

    public void saveDocumentNavigation(ActionEvent e) {
        navi.addNavigationComponent("Lernunterlagen ansehen");
    }

    private void setWrongQuestion() {
        String idStr = Integer.toString(question.getId());
        String teststepID = Integer.toString(navi.getTestStep().getId());
        cookieHelper.setCookie(teststepID, idStr);
    }

    private void removeWrongQuestion() {
        String idStr = Integer.toString(question.getId());
        String teststepID = Integer.toString(navi.getTestStep().getId());
        cookieHelper.removeCookie(teststepID, idStr);
    }
    
    public String getCategoryNameWithArrow(){
        if(this.navi.getCategory() == null){
            return "";
        }else{
            return " > "+this.navi.getCategory().getName();
        }
    }
    
    public void setCategoryNameWithArrow(String s){
        this.categoryNameWithArrow = s;
    }
    public List<Question> getQuestionsOfCategoryPerTestStep(Category c, TestStep s) {
        Category oldCat=navi.getCategory();
        TestStep oldStep=navi.getTestStep();
        navi.setCategory(c);
        navi.setTestStep(s);
        questionsOfCategoryPerTestStep=getQuestionsOfCategoryPerTestStep();
        navi.setCategory(oldCat);
        navi.setTestStep(oldStep);
        return questionsOfCategoryPerTestStep;
    }

    public List<Question> getQuestionsOfCategoryPerTestStep() {
        List<Question> questionList=questionService.getList();
        if (navi.getCategory() != null && navi.getTestStep() != null) {
            Category cat = navi.getCategory();
            TestStep teststep = navi.getTestStep();

            questionsOfCategoryPerTestStep.clear();
            t_qList.clear();
            questionsPerTestStep.clear();

            //nach TestStep selektieren
            for (int idx = 0; idx < t_qService.getList().size(); idx++) {
                if (t_qService.getList().get(idx).getTeststepID() == teststep.getId()) {
                    t_qList.add(t_qService.getList().get(idx));
                }
            }

            for (int idx = 0; idx < t_qList.size(); idx++) {
                for (int quIdx = 0; quIdx < questionList.size(); quIdx++) {
                    if (t_qList.get(idx).getQuestionID() == questionList.get(quIdx).getId()) {
                        questionsPerTestStep.add(questionList.get(quIdx));
                    }
                }
            }

            //nach Kategorie selektieren
            for (int idx = 0; idx < questionsPerTestStep.size(); idx++) {
                if (questionsPerTestStep.get(idx).getCategoryID() == cat.getId()) {
                    questionsOfCategoryPerTestStep.add(questionsPerTestStep.get(idx));
                }
            }
        }
        return questionsOfCategoryPerTestStep;
    }

    private List<Question> getQuestionsPerTestStep() {
        List<Question> questionList=questionService.getList();
        TestStep teststep = navi.getTestStep();
        t_qList.clear();
        questionsPerTestStep.clear();

        //nach TestStep selektieren
        for (int idx = 0; idx < t_qService.getList().size(); idx++) {
            if (t_qService.getList().get(idx).getTeststepID() == teststep.getId()) {
                t_qList.add(t_qService.getList().get(idx));
            }
        }

        for (int idx = 0; idx < t_qList.size(); idx++) {
            for (int quIdx = 0; quIdx < questionList.size(); quIdx++) {
                if (t_qList.get(idx).getQuestionID() == questionList.get(quIdx).getId()) {
                    questionsPerTestStep.add(questionList.get(quIdx));
                }
            }
        }
        return questionsPerTestStep;
    }
    public String navigate(String navigationItem) {
        String wantedLocation = null;
        switch (navigationItem) {
            case "Test auswählen":
                wantedLocation = "/chooseTest.xhtml";
                navi.setTest(null);
                navi.setTestStep(null);
                navi.setCategory(null);
                navi.setQuestion(null);
                resetQuestionForNavigation();
                break;
            case "Teststufe auswählen":
                wantedLocation = "/test.xhtml";
                navi.setTestStep(null);
                navi.setCategory(null);
                navi.setQuestion(null);
                resetQuestionForNavigation();
                break;
            case "Übungsmöglichkeiten auswählen":
                wantedLocation = "/testStep.xhtml";
                navi.setCategory(null);
                navi.setQuestion(null);
                resetQuestionForNavigation();
                break;
            case "Kategorie auswählen":
                wantedLocation = "/practiceQuestions.xhtml";
                navi.setCategory(null);
                navi.setQuestion(null);
                resetQuestionForNavigation();
                break;
            case "Unterlagen ansehen":
                wantedLocation = "/documents.xhtml";
                break;
        }

        int indexOfNaviItem = navi.getNavigationList().indexOf(navigationItem);
        for (int idx = navi.getNavigationList().size() - 1; idx > indexOfNaviItem; idx--) {
            navi.removeNavigationComponent(idx);
        }
        return wantedLocation;
    }
    
    public void clearNavigation() {
        navi.setTest(null);
        navi.setTestStep(null);
        navi.setCategory(null);
        navi.setQuestion(null);
        navi.getNavigationList().clear();
        resetQuestionForNavigation();
    }
    
    
    public String logout() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        session.invalidate();
        clearNavigation();
        return "/login.xhtml";
    }
    
    public void logoutOnBrowserCloseEvent(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        session.invalidate();
        clearNavigation();
    }
}
