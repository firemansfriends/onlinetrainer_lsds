/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import POJO.Test;
import Properties.PathProperties;
import Services.TestService;
import java.io.Serializable;
import javax.faces.event.ActionEvent;
/**
 *
 * @author Lisa
 */

public class TestBean implements Serializable{

    private NavigationBean navi;
    private TestService testservice = TestService.getInstance();
    private PathProperties prop=PathProperties.getInstance();
    
    public void home(ActionEvent event) {
        navi.setTest(null);
    }

    public NavigationBean getNavi() {
        return navi;
    }

    public void setNavi(NavigationBean navi) {
        this.navi = navi;
    }

    public TestService getTestservice() {
        return testservice;
    }

    public void setTestservice(TestService testservice) {
        this.testservice = testservice;
    }

    public void saveNavigation(Test test) {
        navi.setTest(test);
        navi.addNavigationComponent("Teststufe auswählen");
    }
    
    public String changePersonalData(){
        String navigateTo=null;
        return navigateTo;
    }
}
