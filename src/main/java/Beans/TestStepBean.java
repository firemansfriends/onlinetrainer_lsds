/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import POJO.Test;
import POJO.TestStep;
import Services.TestStepService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Lisa
 */
public class TestStepBean implements Serializable{

    private NavigationBean navi;
    private TestStepService teststepservice = TestStepService.getInstance();
    private List<TestStep> testStepsOfTest = new ArrayList<>();

    public void home(ActionEvent event){
        navi.setTest(null);
        navi.setTestStep(null);
    }

    public NavigationBean getNavi() {
        return navi;
    }

    public void setNavi(NavigationBean navi) {
        this.navi = navi;
    }
    
    public TestStepService getTeststepservice() {
        return teststepservice;
    }

    public void setTeststepservice(TestStepService teststepservice) {
        this.teststepservice = teststepservice;
    }
    
    public void saveNavigation(TestStep testStep){
        navi.setTestStep(testStep);
        navi.addNavigationComponent("Übungsmöglichkeiten auswählen");
    }
    
    public List<TestStep> getTestStepsOfTest() {
        List<TestStep> teststepList=teststepservice.getList();
        Test test = navi.getTest();
        testStepsOfTest.clear();
        if (test != null) {
            for (int idx = 0; idx < teststepList.size(); idx++) {
                if (teststepList.get(idx).getTestID() == test.getId()) {
                    testStepsOfTest.add(teststepList.get(idx));
                }
            }
        }
        return testStepsOfTest;
    }
    public List<TestStep> getTestStepsPerTest(Test t){
        Test oldTest=navi.getTest();
        navi.setTest(t);
        List<TestStep> list=getTestStepsOfTest();
        navi.setTest(oldTest);
        return list;
    }
}
