/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Lisa
 */
public class TestloginBean implements Serializable{

    private String username;
    private String password;
    private static final String[] users = {"testuser1:lfvb1", "testuser2:lfvb2", "testuser3:lfvb3"};

    public String login() {
        String loginsuccess = "/testlogin.xhtml";
        for (String user : users) {
            String dbUsername = user.split(":")[0];
            String dbPassword = user.split(":")[1];

            if (dbUsername.equals(username) && dbPassword.equals(password)) {
                loginsuccess="/login.xhtml";
            }
        }
        // Set login ERROR
        FacesMessage msg = new FacesMessage("Login error!", "ERROR MSG");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return loginsuccess;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
