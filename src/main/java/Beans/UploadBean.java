/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import POJO.Answer;
import POJO.Category;
import POJO.Question;
import POJO.Test;
import POJO.TestStep;
import POJO.TestStep_Question;
import Properties.PathProperties;
import Services.AnswerService;
import Services.QuestionService;
import Services.TestStep_QuestionService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Denise
 */

public class UploadBean implements Serializable{

    private UploadedFile uploadedFile;
    private final String firstline = "Fragentext;Fragenart;Eingabehinweis;Feuerwehrnummer;Antworttext 1;Korrekt/Nummerierung;Antworttext 2;Korrekt/Nummerierung;Antworttext 3;Korrekt/Nummerierung;Antworttext 4;Korrekt/Nummerierung;Antworttext 5;Korrekt/Nummerierung;Antworttext 6;Korrekt/Nummerierung;Antworttext 7;Korrekt/Nummerierung;Antworttext 8;Korrekt/Nummerierung;Antworttext 9;Korrekt/Nummerierung;Antworttext 10;Korrekt/Nummerierung";
    private final String endSign = "!END";
    private final int qText = 0, qType = 1, hint = 2, fbNum = 3, aText1 = 4, cN1 = 5, aText2 = 6, cN2 = 7, aText3 = 8, cN3 = 9, aText4 = 10, cN4 = 11, aText5 = 12, cN5 = 13, aText6 = 14, cN6 = 15, aText7 = 16, cN7 = 17, aText8 = 18, cN8 = 19, aText9 = 20, cN9 = 21, aText10 = 22, cN10 = 23;
    private QuestionService questionService = QuestionService.getInstance();
    private AnswerService answerService = AnswerService.getInstance();
    private PathProperties prop=PathProperties.getInstance();
    private String imagePath = prop.getStringProperty(PathProperties.IMAGEPATH);
    private UploadedFile uploadedImage;
    private TestStep_QuestionService tqService = TestStep_QuestionService.getInstance();
    private TestStep currStep = new TestStep("", 0);
    private Test currTest = new Test("", false);
    private Category currCat = new Category("");
    private Question currQuestion = new Question();
    private TestStep step = new TestStep("", 0);
    private final String COMMANDER = "commander", FOUNDINGYEAR = "foundingYear", YOUTHCOMMANDER = "youthCommander", RESCUEEQUIPMENT = "rescueEquipment", SECTION="section";

    public TestStep getStep() {
        return step;
    }

    public void setStep(TestStep step) {
        this.step = step;
    }

    public QuestionService getQuestionService() {
        return questionService;
    }

    public void setQuestionService(QuestionService questionService) {
        this.questionService = questionService;
    }

    public TestStep getCurrStep() {
        return currStep;
    }

    public void setCurrStep(TestStep currStep) {
        this.currStep = currStep;
    }

    public Test getCurrTest() {
        return currTest;
    }

    public void setCurrTest(Test currTest) {
        this.currTest = currTest;
    }

    public Category getCurrCat() {
        return currCat;
    }

    public void setCurrCat(Category currCat) {
        this.currCat = currCat;
    }

    public UploadedFile getUploadedImage() {
        return uploadedImage;
    }

    public void setUploadedImage(UploadedFile uploadedImage) {
        this.uploadedImage = uploadedImage;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public void uploadCSV() {
        if (currTest != null && currStep != null && currCat != null) {
            FacesMessage m = null;
            int lineCnt = 1;
            if (uploadedFile.getFileName().endsWith(".csv")) {//überprüfen ob CSV Datei
                InputStream inputstream = null;
                try {
                    inputstream = uploadedFile.getInputstream(); //inputstream aus uploadedFile holen
                    File temp = File.createTempFile("test", ".csv"); // temporäres File erstellen
                    OutputStream outputStream = new FileOutputStream(temp);
                    int read = 0;
                    byte[] bytes = new byte[1024];
                    while ((read = inputstream.read(bytes)) != -1) {
                        outputStream.write(bytes, 0, read); //in temporäres File schreiben, um nacher das temp-File verwenden zu können
                    }
                    java.io.BufferedReader FileReader = new java.io.BufferedReader(new java.io.FileReader(temp));
                    String line = "";
                    Question q;
                    Answer a = new Answer();
                    List<Answer> ansList = new ArrayList<>();
                    if ((line = FileReader.readLine()) != null && line.contains(firstline)) {//überprüfen, ob erste Ziel stimmt, sonst falsche Datei
                        while (null != (line = FileReader.readLine()) && !line.contains(endSign)) {
                            lineCnt++;
                            String[] parts = line.split(";");
                            int fBNum = Integer.parseInt(parts[fbNum]);
                            if (fBNum > 99999) {
                                m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + ", Feuerwehrnummer darf höchstens 5-stellig sein!", "");
                                return;
                            }
                            int quType = Integer.parseInt(parts[qType]);
                            if (quType == 0) {
                                q = new Question(parts[qText], currCat.getId(), 1, parts[hint]);
                            } else {
                                q = new Question(parts[qText], currCat.getId(), quType, parts[hint]);
                            }
                            if (questionService.insertObject(q)) {
                                tqService.insertObject(new TestStep_Question(currStep.getId(), q.getId()));//damit Frage einer Teststufe zugeordnet wird
                                ansList.clear();
                                if (quType == 0) {//Texteingabe mit bezirks- oder abschnittsspezifischen Antworten
                                    if (fBNum != 0) {
                                        a = new Answer(fBNum, parts[aText1], q.getId(), true, 0);
                                        answerService.insertObject(a);
                                        ansList.add(a);
                                        FileReader.mark(5000);
                                        while (null != (line = FileReader.readLine()) && !line.contains(endSign) && line.startsWith(";;")) {
                                            FileReader.mark(5000);
                                            lineCnt++;
                                            parts = line.split(";");
                                            try {
                                                fBNum = Integer.parseInt(parts[fbNum]);
                                                if (fBNum > 99999) {
                                                    m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + ", Feuerwehrnummer darf höchstens 5-stellig sein!", "");
                                                    for (Answer ans : ansList) {
                                                        answerService.deleteObject(ans);
                                                    }
                                                    tqService.deleteStepQuestion(currStep.getId(), q.getId());
                                                    questionService.deleteObject(q);
                                                    return;
                                                }
                                            } catch (NumberFormatException e) {
                                                m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + ", Feuerwehrnummer falsch!", "");
                                                for (Answer ans : ansList) {
                                                    answerService.deleteObject(ans);
                                                }
                                                tqService.deleteStepQuestion(currStep.getId(), q.getId());
                                                questionService.deleteObject(q);
                                                return;
                                            }
                                            a = new Answer(fBNum, parts[aText1], q.getId(), true, 0);
                                            answerService.insertObject(a);
                                            ansList.add(a);
                                        }
                                        FileReader.reset();
                                    } else {
                                        m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + ", Feuerwehrnummer darf bei Fragenart 0 nicht 0 sein!", "");
                                        tqService.deleteStepQuestion(currStep.getId(), q.getId());
                                        questionService.deleteObject(q);
                                        return;
                                    }
                                }
                                if (quType == 3) {//Bildvergleich
                                    if (Files.exists(Paths.get(imagePath + parts[aText1]))) {
                                        a = new Answer(fBNum, parts[aText1], q.getId(), true, 0);
                                        answerService.insertObject(a);
                                    } else {
                                        tqService.deleteStepQuestion(currStep.getId(), q.getId());
                                        questionService.deleteObject(q);
                                        m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + ", Bild zuerst hochladen!", "");
                                        return;
                                    }

                                }
                                if (quType != 0 && quType != 3) {//bei 1+3  wird nur eine Antwort pro zeile deshalb kein for
                                    for (int idx = aText1; idx < parts.length; idx += 2) {//mehrere Antworten möglich, deshalb for-Schleife
                                        if (!parts[idx].isEmpty()) {
                                            if (quType == 1) {//Texteingabe
                                                if (fBNum == 11) {
                                                    if (parts[idx].equals(this.COMMANDER) || parts[idx].equals(this.YOUTHCOMMANDER) || parts[idx].equals(this.FOUNDINGYEAR) || parts[idx].equals(this.RESCUEEQUIPMENT) || parts[idx].equals(this.SECTION)) {
                                                        a = new Answer(fBNum, parts[idx], q.getId(), true, 0);
                                                    } else {
                                                        tqService.deleteStepQuestion(currStep.getId(), q.getId());
                                                        questionService.deleteObject(q);
                                                        ansList.clear();
                                                        m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + ", falsche Eingabe bei Spalte Antworttext, bei Feuerwehrnummer 11 muss Spalte existieren!", "");
                                                        return;
                                                    }
                                                } else {
                                                    a = new Answer(fBNum, parts[idx], q.getId(), true, 0);
                                                }

                                            }
                                            if (quType == 2) {//MultipleChoice
                                                String c = parts[idx + 1];
                                                if (c.equals("j") || c.equals("n")) {
                                                    boolean correct = true;
                                                    if (c.equals("j")) {
                                                        correct = true;
                                                    }
                                                    if (c.equals("n")) {
                                                        correct = false;
                                                    }
                                                    a = new Answer(fBNum, parts[idx], q.getId(), correct, 0);
                                                    ansList.add(a);
                                                } else {
                                                    for (Answer answer : ansList) {
                                                        answerService.deleteObject(answer);
                                                    }
                                                    tqService.deleteStepQuestion(currStep.getId(), q.getId());
                                                    questionService.deleteObject(q);
                                                    ansList.clear();
                                                    m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + ", falsche Eingabe bei Spalte Korrekt/Nummerierung oder falsche Fragenart!", "");
                                                    return;
                                                }
                                            }
                                            if (quType == 4) {//Nummerierung
                                                String n = parts[idx + 1];
                                                try {
                                                    int num = Integer.parseInt(n);
                                                    a = new Answer(fBNum, parts[idx], q.getId(), true, num);
                                                    ansList.add(a);
                                                } catch (NumberFormatException e) {
                                                    for (Answer answer : ansList) {
                                                        answerService.deleteObject(answer);
                                                    }
                                                    tqService.deleteStepQuestion(currStep.getId(), q.getId());
                                                    questionService.deleteObject(q);
                                                    ansList.clear();
                                                    m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + ", falsche Eingabe bei Spalte Korrekt/Nummerierung oder falsche Fragenart!", "");
                                                    return;
                                                }
                                            }
                                            if (quType == 5) {//Bildauswahl
                                                String c = parts[idx + 1];
                                                boolean exists = Files.exists(Paths.get(imagePath + parts[idx]));
                                                if (exists && (c.equals("j") || c.equals("n"))) {
                                                    boolean correct = true;
                                                    if (c.equals("j")) {
                                                        correct = true;
                                                    }
                                                    if (c.equals("n")) {
                                                        correct = false;
                                                    }
                                                    a = new Answer(fBNum, parts[idx], q.getId(), correct, 0);
                                                    ansList.add(a);
                                                } else {
                                                    for (Answer answer : ansList) {
                                                        answerService.deleteObject(answer);
                                                    }
                                                    tqService.deleteStepQuestion(currStep.getId(), q.getId());
                                                    questionService.deleteObject(q);
                                                    ansList.clear();
                                                    if (exists) {
                                                        m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + ", falsche Eingabe bei Spalte Korrekt/Nummerierung oder falsche Fragenart!", "");
                                                    } else {
                                                        m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + ", zuerst alle Bilder hochladen!", "");
                                                    }
                                                    return;
                                                }
                                            }

                                            if (!answerService.insertObject(a)) {
                                                tqService.deleteStepQuestion(currStep.getId(), q.getId());
                                                questionService.deleteObject(q);
                                                m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + "!", "");
                                                return;
                                            }
                                            if (a.getFbNR() == 11) {
                                                break;
                                            }

                                        }
                                    }
                                }
                            } else {
                                m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fragenart in Zeile " + lineCnt + " falsch!", "");
                                return;
                            }
                        }
                    } else {
                        m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falsche CSV Datei!", "");
                    }

                } catch (IOException ex) {
                    Logger.getLogger(UploadBean.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception e) {
                    m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler in Zeile " + lineCnt + "!", "");
                } finally {
                    try {
                        inputstream.close();
                        if (m != null) {
                            FacesContext.getCurrentInstance().addMessage(null, m);
                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Import fehlerfrei abgeschlossen!", ""));
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(UploadBean.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                if (uploadedFile.getFileName().isEmpty()) {
                    m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Datei auswählen!", "");
                } else {
                    m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falsches Dateiformat!", "");
                }
                FacesContext.getCurrentInstance().addMessage(null, m);
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Zuerst Test, Teststufe und Kategorie auswählen", ""));
        }
    }

    public void uploadImage() {
        String name = uploadedImage.getFileName();
        if (name.endsWith(".png") || name.endsWith(".PNG") || name.endsWith(".jpg") || name.endsWith(".JPG") || name.endsWith(".jpeg")) {
            try {
                Path path = Paths.get(imagePath + uploadedImage.getFileName());
                Files.copy(uploadedImage.getInputstream(), path, REPLACE_EXISTING);
            } catch (IOException ex) {
                Logger.getLogger(UploadBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Falscher Dateityp!", ""));
        }

    }

    public void saveQuestion(Question q) {
        questionService.updateObject(q);
    }

    public void saveAnswer(Answer a) {
        answerService.updateObject(a);
    }

    public void deleteQuStep(Question q) {
        tqService.deleteStepQuestion(currStep.getId(), q.getId());
    }

    public void deleteAnswer(Answer a) {
        answerService.deleteObject(a);
    }

    public Question getCurrQuestion() {
        return currQuestion;
    }

    public void setCurrQuestion(Question currQuestion) {
        this.currQuestion = currQuestion;       
    }

    public void addQuestionToStep(Question q) {
        if (step != null) {
            for (TestStep_Question tq : tqService.getList()) {
                if (tq.getQuestionID() == q.getId() && tq.getTeststepID() == step.getId()) {
                    return;
                }
            }
            tqService.insertObject(new TestStep_Question(step.getId(), q.getId()));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Zuerst Stufe auswählen, in die die Frage eingefügt werden soll!", ""));
        }
    }

    public AnswerService getAnswerService() {
        return answerService;
    }

    public void setAnswerService(AnswerService answerService) {
        this.answerService = answerService;
    }
    public String getBackground(Question question){
        if(currQuestion.getId()!=0&&question.getId()==currQuestion.getId()){
            return "background: yellow";
        }
        return "background: white";
    }
    public void changeCorrect(Answer answer){
        answerService.updateObject(answer);
    }

}
