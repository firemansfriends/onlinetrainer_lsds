/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import POJO.Firebrigade;
import POJO.User;
import POJO.Userright;
import Services.FirebrigadeService;
import Services.UserService;
import Services.UserrightService;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Lisa
 */
public class UsermanagementBean implements Serializable{

    private String username = null;
    private String password = null;
    private String email = null;
    private Firebrigade firebrigade = null;
    private ArrayList<Firebrigade> firebrigades = new ArrayList<>();
    private Userright userright = null;
    private User currentUser = new User();
    private int currentUserPassword;
    private Firebrigade currentfirebrigade = new Firebrigade();
    private Userright currentUserright = new Userright();
    private ArrayList<Userright> userrights = new ArrayList<>();
    private UserService us = UserService.getInstance();
    private UserrightService urs = UserrightService.getInstance();
    private FirebrigadeService fs = FirebrigadeService.getInstance();
    private LoginBean lB;

    public void insert() {
        if (username != null && !(username.equals(""))
                && password != null && !(password.equals(""))
                && firebrigade != null && userright != null) {
            if (!isUsernameThere()) {
                us.insertObject(new User(firebrigade.getId(), username, password.hashCode(), email, userright.getId()));
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                        "Benutzer wurde erfolgreich hinzugefügt!"));
                username = null;
                password = null;
                email = null;
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                    "Bitte geben Sie alle gefordeten Daten an!"));
        }
    }

    public void delete(User u) {
        if (lB.getLs().getCurrentUser().getUsername().equals(u.getUsername())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                    "Benutzer kann nicht gelöscht werden!"));
        } else {
            String uname = u.getUsername();
            us.deleteObject(u);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                    "Benutzer " + uname + " wurde erfolgreich gelöscht"));
        }
    }

    public void edit(User u) {
        currentUser.setUsername(u.getUsername());
        if (lB.getLs().getCurrentUser().getUsername().equals(this.currentUser.getUsername())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                    "Benutzer kann nicht verändert werden!"));
            currentUser.setUsername(" ");
        } else {
            this.currentUserPassword = u.getPassword();
            currentUser.setId(u.getId());
            currentUser.setEmail(u.getEmail());
            currentUser.setFirebrigadeID(u.getFirebrigadeID());
            currentUser.setUserrightID(u.getUserrightID());
            this.currentfirebrigade.setId(u.getFirebrigadeID());
            this.currentfirebrigade.setLocation(fs.getLocationToFirebrigadeID(u.getFirebrigadeID()));
            this.currentUserright.setId(u.getUserrightID());
            this.currentUserright.setUserright(urs.getUserrightToID(u.getUserrightID()));
        }
    }

    public void save() {
        currentUser.setPassword(currentUserPassword);
        currentUser.setFirebrigadeID(currentfirebrigade.getId());
        currentUser.setUserrightID(currentUserright.getId());
        this.us.updateObject(currentUser);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                currentUser.getUsername() + " wurde erfolgreich geändert"));

        currentUser.setId(0);
        currentUser.setUsername(" ");
        currentUser.setEmail(" ");
        currentUser.setFirebrigadeID(0);
        this.currentfirebrigade.setId(0);
        this.currentfirebrigade.setLocation("");
        currentUser.setUserrightID(0);
        this.currentUserright.setId(0);
        this.currentUserright.setUserright("");
    }

    public void cancel() {
        currentUser.setId(0);
        currentUser.setUsername(" ");
        currentUser.setEmail(" ");
        currentUser.setFirebrigadeID(0);
        currentUser.setUserrightID(0);
        this.currentfirebrigade.setId(0);
        this.currentfirebrigade.setLocation("");
        this.currentUserright.setId(0);
        this.currentUserright.setUserright("");
    }

    public boolean isUsernameThere() {
        boolean ret = false;
        for (int idx = 0; idx < us.getList().size(); idx++) {
            if (username.equals(us.getList().get(idx).getUsername())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                        "Benutzername ist bereits vorhanden"));
                ret = true;
                break;
            } else {
                ret = false;
            }
        }
        return ret;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Firebrigade getFirebrigade() {
        return firebrigade;
    }

    public void setFirebrigade(Firebrigade firebrigade) {
        this.firebrigade = firebrigade;
    }

    public Userright getUserright() {
        return userright;
    }

    public void setUserright(Userright userright) {
        this.userright = userright;
    }

    public ArrayList<Firebrigade> getFirebrigades() {
        return firebrigades;
    }

    public void setFirebrigades(ArrayList<Firebrigade> firebrigades) {
        this.firebrigades = firebrigades;
    }

    public ArrayList<Userright> getUserrights() {
        return userrights;
    }

    public void setUserrights(ArrayList<Userright> userrights) {
        this.userrights = userrights;
    }

    public UserService getUs() {
        return us;
    }

    public void setUs(UserService us) {
        this.us = us;
    }

    public FirebrigadeService getFs() {
        return fs;
    }

    public void setFs(FirebrigadeService fs) {
        this.fs = fs;
    }

    public UserrightService getUrs() {
        return urs;
    }

    public void setUrs(UserrightService urs) {
        this.urs = urs;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public Firebrigade getCurrentfirebrigade() {
        return currentfirebrigade;
    }

    public void setCurrentfirebrigade(Firebrigade currentfirebrigade) {
        this.currentfirebrigade = currentfirebrigade;
    }

    public Userright getCurrentUserright() {
        return currentUserright;
    }

    public void setCurrentUserright(Userright currentUserright) {
        this.currentUserright = currentUserright;
    }

    public LoginBean getlB() {
        return lB;
    }

    public void setlB(LoginBean lB) {
        this.lB = lB;
    }

}
