/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Converter;

import POJO.Category;
import Services.CategoryService;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Denise
 */

public class CategoryConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        CategoryService service=CategoryService.getInstance();
        String[] catParts=string.split(";");
        Category c=service.readObject(Integer.parseInt(catParts[1]));
        return c;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if(o==null){
            return " "+";"+Integer.toString(0);
        }
        Category c=(Category)o; 
        return c.getName()+";"+Integer.toString(c.getId());
    }

}
