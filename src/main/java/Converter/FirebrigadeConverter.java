/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Converter;

import POJO.Firebrigade;
import Services.FirebrigadeService;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Lisa
 */
public class FirebrigadeConverter implements Converter{
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        FirebrigadeService service = FirebrigadeService.getInstance();
        String[] firebrigade=string.split("-");
        Firebrigade c=service.readObject(Integer.parseInt(firebrigade[0]));
        return c;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        Firebrigade f=(Firebrigade)o; 
        return f.getId()+"-"+f.getLocation();
    }
}
