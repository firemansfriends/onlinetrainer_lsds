/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 *
 * @author Denise
 */
public class NumberConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        try{
            return Integer.parseInt(string);
        }catch(NumberFormatException e){
            FacesMessage m=new FacesMessage("Ungültige Zeichen, nur Zahlen eingeben!");
            throw new ConverterException(m);
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        
        return o.toString();
        
    }

}
