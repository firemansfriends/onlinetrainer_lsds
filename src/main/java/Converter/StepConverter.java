/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Converter;

import POJO.TestStep;
import Services.TestStepService;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Denise
 */
public class StepConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        TestStepService s= TestStepService.getInstance();
        return s.readObject(Integer.parseInt(string));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if(o==null){
            return Integer.toString(0);
        }
        TestStep t=(TestStep)o;
        return Integer.toString(t.getId()); 
    }

}
