/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Converter;

import POJO.Userright;
import Services.UserrightService;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Lisa
 */
public class UserrightConverter implements Converter{
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        UserrightService service = UserrightService.getInstance();
        String[] userright=string.split("-");
        Userright c=service.readObject(Integer.parseInt(userright[0]));
        return c;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        Userright u=(Userright)o; 
        return u.getId()+"-"+u.getUserright();
    }
}
