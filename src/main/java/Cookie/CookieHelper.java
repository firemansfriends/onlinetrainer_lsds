/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cookie;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;

/**
 *
 * @author Denise
 */
public class CookieHelper {
    private final String seperator="a";

    public void setCookie(String name, String value) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("maxAge", (int) TimeUnit.DAYS.toSeconds(365));
        Cookie c = getCookie(name);
        if (c == null) {//wenn es Cookie nicht gibt, wird es angelegt mit id und "a" als Trennzeichen als Wert
            FacesContext.getCurrentInstance().getExternalContext().addResponseCookie(name, seperator + value + seperator, properties);
        } else {//es gibt schon ein Cookie
            String idString = c.getValue();
            if (!idString.contains(seperator + value + seperator)) {//überprüfen, ob die id schon im Cookiewert vorhanden ist, wenn nicht, wird id hinzugefügt
                StringBuilder s = new StringBuilder(idString);
                if(idString.isEmpty()){
                    s.append(seperator+value + seperator);
                }else{
                    s.append(value + seperator);
                }                               
                FacesContext.getCurrentInstance().getExternalContext().addResponseCookie(name, s.toString(), properties);
            }
        }
    }

    public void removeCookie(String name, String value) {
        Cookie cookie = getCookie(name);
        if (cookie != null) {
            String cValue = cookie.getValue();
            if (cValue.contains(seperator + value + seperator)) {
                Map<String, Object> properties = new HashMap<>();
                properties.put("maxAge", (int) TimeUnit.DAYS.toSeconds(365));
                String[] cparts = cValue.split(seperator + value + seperator);
                String newVal="";
                if (cparts.length == 1) {
                    newVal = cparts[0] + seperator;
                } 
                if (cparts.length == 0) {
                    newVal="";
                } 
                if (cparts.length == 2) {
                    newVal = cparts[0] + seperator + cparts[1];
                }
                FacesContext.getCurrentInstance().getExternalContext().addResponseCookie(name, newVal, properties);
            }
        }
    }

    public Cookie getCookie(String name) {
        Map<String, Object> m;
        m = FacesContext.getCurrentInstance().getExternalContext().getRequestCookieMap();

        return (Cookie) m.get(name);
    }
}
