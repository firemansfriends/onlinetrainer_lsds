/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;

/**
 *
 * @author Lisa
 */
public interface DAO<T> {

    public boolean create(T t);
    public T read(int id);
    public void update(T t);
    public boolean delete(T t);
    public List<T> list();
}
