/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Email;

import java.security.SecureRandom;

/**
 *
 * @author Lisa
 */
public class PasswordGenerator {

    final String all = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    SecureRandom random = new SecureRandom();

    String randomString() {
        StringBuilder sb = new StringBuilder(8);
        for (int idx = 0; idx < 8; idx++) {
            sb.append(all.charAt(random.nextInt(all.length())));
        }
        return sb.toString();
    }

}
