/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Email;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Lisa
 */
public class SendingEmails {
    
    private PasswordGenerator passwordGenerator = new PasswordGenerator();
    private String generatedPassword = passwordGenerator.randomString();

    public void sendMail(String r) {
        String sender = "lfvb-burgenland@gmx.at";
        String password = "lfvbburgenland";
        String receiver = r;

        final Properties properties = new Properties();

        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.host", "mail.gmx.net");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.user", sender);
        properties.put("mail.smtp.password", password);
        properties.put("mail.smtp.starttls.enable", "true");

        Session mailSession = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(properties.getProperty("mail.smtp.user"),
                        properties.getProperty("mail.smtp.password"));
            }
        });
        try {
            Message message = new MimeMessage(mailSession);
            InternetAddress addressTo = new InternetAddress(receiver);
            message.setRecipient(Message.RecipientType.TO, addressTo);
            message.setFrom(new InternetAddress(sender));
            message.setSubject("Passwort zurücksetzen");
            message.setContent("Ihr neues Passwort lautet: "+generatedPassword+ 
                    " Verwenden Sie dieses um sich anzumelden und aendern Sie es nach "
                    + "Bedarf in den persoenlichen Daten.", "text/plain");
            Transport.send(message);
        } catch (MessagingException me) {
            me.printStackTrace();
        }
    }

    public String getGeneratedPassword() {
        return generatedPassword;
    }

    public void setGeneratedPassword(String generatedPassword) {
        this.generatedPassword = generatedPassword;
    }
}
