/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Filter;

/**
 *
 * @author Lisa
 */
import java.io.IOException;
import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Lisa
 */
public class LoginFilter implements Filter {

    private static final String AJAX_REDIRECT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<partial-response><redirect url=\"%s\"></redirect></partial-response>";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(true);
        String loginURL = request.getContextPath() + "/faces/login.xhtml";
        String adminloginURL = request.getContextPath() + "/faces/adminlogin.xhtml";
        String enterMailURL = request.getContextPath() + "/faces/enterMail.xhtml";
        String superUserStatisticsURL = request.getContextPath() + "/faces/statisticsSuperuser.xhtml";
        String adminStatisticsURL = request.getContextPath() + "faces/statisticsAdmin.xhtml";
        String homeURL = request.getContextPath() + "faces/home.xhtml";
        String usermanagementURL = request.getContextPath() + "faces/usermanagement.xhtml";
        String editDataURL = request.getContextPath() + "faces/editdata.xhtml";
        String requestURI = request.getRequestURI();

        try {
            boolean isLoggedIn = (Boolean) session.getAttribute("isLoggedIn");
        } catch (NullPointerException n) {
            session.setAttribute("isLoggedIn", false);
        }

        Integer userright = (Integer) session.getAttribute("userrightID");
        if (userright == null) {
            session.setAttribute("userrightID", -1);
        }
        Integer userrightID = (Integer) session.getAttribute("userrightID");
        boolean isLoggedIn = (Boolean) session.getAttribute("isLoggedIn");

        boolean loginRequest = request.getRequestURI().equals(loginURL);
        boolean adminloginRequest = request.getRequestURI().equals(adminloginURL);
        boolean superuserStatistics = request.getRequestURI().equals(superUserStatisticsURL);
        boolean adminStatistics = request.getRequestURI().equals(adminStatisticsURL);
        boolean home = request.getRequestURI().equals(homeURL);
        boolean usermanagement = request.getRequestURI().equals(usermanagementURL);
        boolean editdata = request.getRequestURI().equals(editDataURL);
        boolean enterMail = request.getRequestURI().equals(enterMailURL);

        boolean ajaxRequest = "partial/ajax".equals(request.getHeader("Faces-Request"));
        boolean resourceRequest = requestURI.startsWith(request.getContextPath() + "/faces" + ResourceHandler.RESOURCE_IDENTIFIER);

        if (isLoggedIn || loginRequest || resourceRequest || adminloginRequest || enterMail) {
            chain.doFilter(request, response); // So, just continue request.
        } else if (ajaxRequest) {
            response.setContentType("text/xml");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().printf(AJAX_REDIRECT_XML, loginURL); // So, return special XML response instructing JSF ajax to send a redirect.
        } else {
            response.sendRedirect(loginURL); // So, just perform standard synchronous redirect.
        }
        /*if (isLoggedIn) {
         switch (userrightID) {
         case 1:
         if (adminStatistics || home || usermanagement || editdata || adminloginRequest || loginRequest || resourceRequest) {
         chain.doFilter(request, response);
         } else if (ajaxRequest) {
         response.setContentType("text/xml");
         response.setCharacterEncoding("UTF-8");
         response.getWriter().printf(AJAX_REDIRECT_XML, loginURL);
         } else {
         response.sendRedirect(loginURL);
         }
         break;
         case 2:
         if (superuserStatistics || home || adminloginRequest || loginRequest || resourceRequest) {
         chain.doFilter(request, response);
         } else if (ajaxRequest) {
         response.setContentType("text/xml");
         response.setCharacterEncoding("UTF-8");
         response.getWriter().printf(AJAX_REDIRECT_XML, loginURL);
         } else {
         response.sendRedirect(loginURL);
         }
         break;
         case 3:
         if (home || adminloginRequest || loginRequest || resourceRequest) {
         chain.doFilter(request, response);
         } else if (ajaxRequest) {
         response.setContentType("text/xml");
         response.setCharacterEncoding("UTF-8");
         response.getWriter().printf(AJAX_REDIRECT_XML, loginURL);
         } else {
         response.sendRedirect(loginURL);
         }
         break;
         case -1:
         break;
         }
         } else {
         if (resourceRequest||loginRequest||adminloginRequest) {
         chain.doFilter(request, response);
         }else if (ajaxRequest) {
         response.setContentType("text/xml");
         response.setCharacterEncoding("UTF-8");
         response.getWriter().printf(AJAX_REDIRECT_XML, loginURL); // So, return special XML response instructing JSF ajax to send a redirect.
         } else {
         response.sendRedirect(loginURL); // So, just perform standard synchronous redirect.
         }
         }*/
    }

    @Override
    public void destroy() {

    }

}
