/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;

import DAO.AnswerDAO;
import POJO.Answer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class AnswerJDBCDAO extends JDBCBaseDAO<Answer> implements AnswerDAO{
    public AnswerJDBCDAO() {
        super("answers");
    }

    @Override
    protected Answer getPojoFromResultSet(ResultSet result) throws SQLException {
        Answer a = new Answer(result.getInt("FirebrigadeNr"),result.getString("answertext"), result.getInt("questionID"), result.getBoolean("correct"), result.getInt("numbering"));
        a.setId(result.getInt("id"));
        return a;
    }

    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET FirebrigadeNr=?, answertext=?, questionID=?, correct=?, numbering=? WHERE id=?";   
        return sql;
    }

    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (FirebrigadeNr, answertext, questionID, correct, numbering) " + "VALUES (?,?,?,?,?)";
        return sql;
    }

    @Override
    protected void mapPojoToStatementUpdate(Answer t, PreparedStatement stmt) throws SQLException {
        stmt.setInt(1, t.getFbNR());
        stmt.setString(2, t.getText());
        stmt.setInt(3, t.getQuestionID());
        stmt.setBoolean(4,t.getCorrect());
        stmt.setInt(5, t.getNumbering());
        stmt.setInt(6,t.getId());
    }

    @Override
    protected void mapPojoToStatementInsert(Answer t, PreparedStatement stmt) throws SQLException {
        stmt.setInt(1, t.getFbNR());
        stmt.setString(2, t.getText());
        stmt.setInt(3, t.getQuestionID());
        stmt.setBoolean(4,t.getCorrect());
        stmt.setInt(5, t.getNumbering());
    }
}
