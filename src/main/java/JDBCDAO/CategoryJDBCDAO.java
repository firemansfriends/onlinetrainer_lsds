/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;
import DAO.CategoryDAO;
import POJO.Category;
import java.sql.*;

/**
 *
 * @author Lisa
 */
public class CategoryJDBCDAO extends JDBCBaseDAO<Category> implements CategoryDAO{
    public CategoryJDBCDAO() {
        super("categories");
    }

    @Override
    protected Category getPojoFromResultSet(ResultSet result) throws SQLException {
        Category c = new Category(result.getString("catname"));
        c.setId(result.getInt("id"));
        return c;
    }

    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET catname=? WHERE id=?";   
        return sql;
    }
    @Override
    protected void mapPojoToStatementUpdate(Category c, PreparedStatement stmt) throws SQLException{
        stmt.setString(1, c.getName());
        stmt.setInt(2, c.getId());
    }
    @Override
    protected void mapPojoToStatementInsert(Category c, PreparedStatement stmt) throws SQLException{
        stmt.setString(1, c.getName());
    }

    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (catname) " + "VALUES (?)";
        return sql;
    }
}
