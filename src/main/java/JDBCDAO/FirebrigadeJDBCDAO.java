/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;
import DAO.FirebrigadeDAO;
import POJO.Firebrigade;
import java.sql.*;


public class FirebrigadeJDBCDAO extends JDBCBaseDAO<Firebrigade> implements FirebrigadeDAO{
    public FirebrigadeJDBCDAO() {
        super("firebrigade");
    }

    @Override
    protected Firebrigade getPojoFromResultSet(ResultSet result) throws SQLException {
        Firebrigade f=new Firebrigade(result.getInt("firebrigadenr"), result.getString("location"),result.getString("commander"),
        result.getString("youthCommander"), result.getString("foundingYear"), result.getString("rescueEquipment"));
        f.setId(result.getInt("id"));
        return f;
    }

    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET firebrigadenr=?, location=?, "
        + "commander=?, youthCommander=?, foundingYear=?, rescueEquipment=? WHERE id=?";   
        return sql;
    }
    @Override
    protected void mapPojoToStatementUpdate(Firebrigade f, PreparedStatement stmt) throws SQLException{
        stmt.setInt(1, f.getFirebrigadenumber());
        stmt.setString(2, f.getLocation());
        stmt.setString(3, f.getCommander());
        stmt.setString(4, f.getYouthCommander());
        stmt.setString(5, f.getFoundingYear());
        stmt.setString(6, f.getRescueEquipment());
        stmt.setInt(7, f.getId());
    }
    @Override
    protected void mapPojoToStatementInsert(Firebrigade f, PreparedStatement stmt) throws SQLException{
        stmt.setInt(1, f.getFirebrigadenumber());
        stmt.setString(2, f.getLocation());
        stmt.setString(3, f.getCommander());
        stmt.setString(4, f.getYouthCommander());
        stmt.setString(5, f.getFoundingYear());
        stmt.setString(6, f.getRescueEquipment());
    }

    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (firebrigadenr, location, commander, youthCommander, foundingYear, rescueEquipment) " + "VALUES (?,?,?,?,?,?)";
        return sql;
    }
}
