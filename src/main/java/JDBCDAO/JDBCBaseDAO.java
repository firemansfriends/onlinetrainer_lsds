/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JDBCDAO;

import Managers.ConnectionManager;
import POJO.Identifiable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lisa
 */
public abstract class JDBCBaseDAO<T extends Identifiable> {

    protected abstract T getPojoFromResultSet(ResultSet result) throws SQLException;

    protected abstract String getUpdateString() throws SQLException;

    protected abstract String getInsertString() throws SQLException;

    protected abstract void mapPojoToStatementUpdate(T t, PreparedStatement stmt) throws SQLException;

    protected abstract void mapPojoToStatementInsert(T t, PreparedStatement stmt) throws SQLException;

    private final String TABLENAME;

    public JDBCBaseDAO(String tablename) {
        this.TABLENAME = tablename;
    }

    public String getTablename() {
        return TABLENAME;
    }

    private PreparedStatement createReadPreparedStatement(Connection c,int id) throws SQLException {
        String sql = "SELECT * FROM " + TABLENAME + " WHERE id = ? LIMIT 1";
        PreparedStatement stmt = c.prepareStatement(sql);
        stmt.setInt(1, id);
        return stmt;
    }

    private PreparedStatement createDeletePreparedStatement(Connection c,int id) throws SQLException {
        String sql = "DELETE FROM " + TABLENAME + " WHERE id = ?";
        PreparedStatement stmt = c.prepareStatement(sql);
        stmt.setInt(1, id);
        return stmt;
    }

    private PreparedStatement createUpdatePreparedStatement(Connection c,T t) throws SQLException {
        String sql = getUpdateString();
        PreparedStatement stmt = c.prepareStatement(sql);
        mapPojoToStatementUpdate(t, stmt);
        return stmt;
    }

    private PreparedStatement createCreatePreparedStatement(Connection c, T t) throws SQLException {
        String sql = getInsertString();
        PreparedStatement stmt = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        mapPojoToStatementInsert(t, stmt);
        return stmt;
    }

    public T read(int id) {
        T t = null;
        try (Connection c = ConnectionManager.getInst().getConn();
                PreparedStatement stmt = createReadPreparedStatement(c,id);
                ResultSet result = stmt.executeQuery()) {

            if (result.next()) {
                t = getPojoFromResultSet(result);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return t;
    }

    public void update(T t) {
        try (Connection c = ConnectionManager.getInst().getConn();
                PreparedStatement stmt = createUpdatePreparedStatement(c,t)) {
            if (t.getId() < 0) {
                return;
            }
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<T> list() {
        String sql = "SELECT * FROM " + TABLENAME;
        List<T> results = new ArrayList<>();
        try (Connection c = ConnectionManager.getInst().getConn();
                PreparedStatement stmt = c.prepareStatement(sql);
                ResultSet result = stmt.executeQuery()) {

            while (result.next()) {
                results.add(getPojoFromResultSet(result));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return results;
    }

    public boolean delete(T t) {

        try (Connection c = ConnectionManager.getInst().getConn();
                PreparedStatement stmt = createDeletePreparedStatement(c,t.getId())) {
            if (t.getId() < 0) {
                return true;
            }
            stmt.executeUpdate();
        } catch (SQLException ex) {
            return false;
        }
        return true;
    }

    public boolean create(T t) {
        try (Connection c = ConnectionManager.getInst().getConn();
                PreparedStatement stmt = createCreatePreparedStatement(c,t)) {
            if (t.getId() > 0) {
                return true;
            }
            int updated = stmt.executeUpdate();
            if (updated == 1) {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                if (generatedKeys.next()) {
                    t.setId(generatedKeys.getInt(1));
                }
            }
        } catch (SQLException ex) {
            return false;
        }
        return true;

    }
}
