/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;

import DAO.QuestionDAO;
import POJO.Question;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class QuestionJDBCDAO extends JDBCBaseDAO<Question> implements QuestionDAO{
    public QuestionJDBCDAO() {
        super("questions");
    }

    @Override
    protected Question getPojoFromResultSet(ResultSet result) throws SQLException {
        Question q = new Question(result.getString("questiontext"), result.getInt("catID"), result.getInt("questionTypeID"),
        result.getString("hint"));
        q.setId(result.getInt("id"));
        return q;
    }
    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET questiontext=?, catID=?, questionTypeID=?, hint=?  WHERE id=?";   
        return sql;
    }
    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (questiontext, catID, questionTypeID, hint) " + "VALUES (?,?,?,?)";
        return sql;
    }
    @Override
    protected void mapPojoToStatementUpdate(Question t, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, t.getText());
        stmt.setInt(2, t.getCategoryID());
        stmt.setInt(3, t.getQuestionTypeID());
        stmt.setString(4, t.getHint());
        stmt.setInt(5, t.getId());
    }
    @Override
    protected void mapPojoToStatementInsert(Question t, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, t.getText());
        stmt.setInt(2, t.getCategoryID());
        stmt.setInt(3, t.getQuestionTypeID());
        stmt.setString(4, t.getHint());
    }
}
 
