/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;
import DAO.QuestionTypeDAO;
import POJO.QuestionType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



public class QuestionTypeJDBCDAO extends JDBCBaseDAO<QuestionType> implements QuestionTypeDAO{

    public QuestionTypeJDBCDAO() {
        super("questiontype");
    }
    @Override
    protected QuestionType getPojoFromResultSet(ResultSet result) throws SQLException {
        QuestionType qt = new QuestionType(result.getString("questiontypename"));
        qt.setId(result.getInt("id"));
        return qt;
    }

    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET QuestionTypeName=? WHERE id=?";   
        return sql;
    }

    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (QuestionTypeName) " + "VALUES (?)";
        return sql;
    }

    @Override
    protected void mapPojoToStatementUpdate(QuestionType t, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, t.getName());
        stmt.setInt(2, t.getId());
    }

    @Override
    protected void mapPojoToStatementInsert(QuestionType t, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, t.getName());
    }
} 
