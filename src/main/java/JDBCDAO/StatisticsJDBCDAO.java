/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JDBCDAO;

import DAO.StatisticsDAO;
import POJO.Statistics;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.sql.Date;

/**
 *
 * @author Lisa
 */
public class StatisticsJDBCDAO extends JDBCBaseDAO<Statistics> implements StatisticsDAO {

    public StatisticsJDBCDAO() {
        super("statistics");
    }

    LocalDate toLocalDate(Date date) {
        return date.toLocalDate();
    }
    
    Date toDate(LocalDate localDate){
        return java.sql.Date.valueOf( localDate );
    }

    @Override
    protected Statistics getPojoFromResultSet(ResultSet result) throws SQLException {
        Statistics s = new Statistics(result.getInt("firebrigadeID"), result.getInt("counter"), toLocalDate(result.getDate("actualDate")));
        s.setId(result.getInt("id"));
        return s;
    }

    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET firebrigadeID=?, counter=?, actualDate=? WHERE id=?";
        return sql;
    }

    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (firebrigadeID,counter,actualDate) " + "VALUES (?,?,?)";
        return sql;
    }

    @Override
    protected void mapPojoToStatementUpdate(Statistics t, PreparedStatement stmt) throws SQLException {
        stmt.setInt(1, t.getFirebrigadeID());
        stmt.setInt(2, t.getCounter());
        stmt.setDate(3, toDate(t.getActualDate()));
        stmt.setInt(4, t.getId());
    }

    @Override
    protected void mapPojoToStatementInsert(Statistics t, PreparedStatement stmt) throws SQLException {
        stmt.setInt(1, t.getFirebrigadeID());
        stmt.setInt(2, t.getCounter());
        stmt.setDate(3, toDate(t.getActualDate()));
    }
}
