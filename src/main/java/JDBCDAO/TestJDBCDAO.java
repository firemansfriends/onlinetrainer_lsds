/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;

import DAO.TestDAO;
import POJO.Test;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Lisa
 */
public class TestJDBCDAO extends JDBCBaseDAO<Test> implements TestDAO{
    public TestJDBCDAO() {
        super("test");
    }

    @Override
    protected Test getPojoFromResultSet(ResultSet result) throws SQLException {
        Test t = new Test(result.getString("name"), result.getBoolean("active"));
        t.setId(result.getInt("id"));
        return t;
    }
    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET name=?, active=? WHERE id=?";   
        return sql;
    }
    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (name, active) " + "VALUES (?,?)";
        return sql;
    }
    @Override
    protected void mapPojoToStatementUpdate(Test t, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, t.getName());
        stmt.setBoolean(2, t.isActive());
        stmt.setInt(3, t.getId());
    }
    @Override
    protected void mapPojoToStatementInsert(Test t, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, t.getName());
        stmt.setBoolean(2, t.isActive());
    }
}
