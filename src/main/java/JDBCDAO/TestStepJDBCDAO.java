/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;

import DAO.TestStepDAO;
import POJO.TestStep;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Lisa
 */
public class TestStepJDBCDAO extends JDBCBaseDAO<TestStep> implements TestStepDAO{
    public TestStepJDBCDAO() {
        super("teststep");
    }

    @Override
    protected TestStep getPojoFromResultSet(ResultSet result) throws SQLException {
        TestStep t = new TestStep(result.getString("name"),result.getInt("testid"));
        t.setId(result.getInt("id"));
        return t;
    }
    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET name=?, testid=? WHERE id=?";   
        return sql;
    }
    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (name,testid) " + "VALUES (?,?)";
        return sql;
    }
    @Override
    protected void mapPojoToStatementUpdate(TestStep t, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, t.getName());
        stmt.setInt(2, t.getTestID());
        stmt.setInt(3, t.getId());
    }
    @Override
    protected void mapPojoToStatementInsert(TestStep t, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, t.getName());
        stmt.setInt(2, t.getTestID());
    }
}
