/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;

import DAO.TestStep_CategoryDAO;
import POJO.TestStep_Category;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Lisa
 */
public class TestStep_CategoryJDBCDAO extends JDBCBaseDAO<TestStep_Category> implements TestStep_CategoryDAO{
    public TestStep_CategoryJDBCDAO() {
        super("teststep_category");
    }

    @Override
    protected TestStep_Category getPojoFromResultSet(ResultSet result) throws SQLException {
        TestStep_Category t = new TestStep_Category(result.getInt("teststepid"),result.getInt("categoryid"));
        t.setId(result.getInt("id"));
        return t;
    }
    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET teststepid=?, categoryid=? WHERE id=?";   
        return sql;
    }
    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (teststepid,categoryid) " + "VALUES (?,?)";
        return sql;
    }
    @Override
    protected void mapPojoToStatementUpdate(TestStep_Category t, PreparedStatement stmt) throws SQLException {
        stmt.setInt(1, t.getTestStepID());
        stmt.setInt(2, t.getCategoryID());
        stmt.setInt(3, t.getId());
    }
    @Override
    protected void mapPojoToStatementInsert(TestStep_Category t, PreparedStatement stmt) throws SQLException {
        stmt.setInt(1, t.getTestStepID());
        stmt.setInt(2, t.getCategoryID());
    }
}
