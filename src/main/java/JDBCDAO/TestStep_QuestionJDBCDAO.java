/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;

import DAO.TestStep_QuestionDAO;
import POJO.TestStep_Question;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Lisa
 */
public class TestStep_QuestionJDBCDAO extends JDBCBaseDAO<TestStep_Question> implements TestStep_QuestionDAO{
    public TestStep_QuestionJDBCDAO() {
        super("teststep_question");
    }

    @Override
    protected TestStep_Question getPojoFromResultSet(ResultSet result) throws SQLException {
        TestStep_Question t = new TestStep_Question(result.getInt("teststepid"),result.getInt("questionid"));
        t.setId(result.getInt("id"));
        return t;
    }
    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET teststepid=?, questionid=? WHERE id=?";   
        return sql;
    }
    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (teststepid,questionid) " + "VALUES (?,?)";
        return sql;
    }
    @Override
    protected void mapPojoToStatementUpdate(TestStep_Question t, PreparedStatement stmt) throws SQLException {
        stmt.setInt(1, t.getTeststepID());
        stmt.setInt(2, t.getQuestionID());
        stmt.setInt(3, t.getId());
    }
    @Override
    protected void mapPojoToStatementInsert(TestStep_Question t, PreparedStatement stmt) throws SQLException {
        stmt.setInt(1, t.getTeststepID());
        stmt.setInt(2, t.getQuestionID());
    }
}
