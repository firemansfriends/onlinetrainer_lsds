/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;

import DAO.UserDAO;
import POJO.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Lisa
 */
public class UserJDBCDAO extends JDBCBaseDAO<User> implements UserDAO{

    public UserJDBCDAO() {
        super("user");
    }
    
    @Override
    protected User getPojoFromResultSet(ResultSet result) throws SQLException {
        User u = new User(result.getInt("firebrigadeID"), 
                result.getString("username"), result.getInt("password"), result.getString("email"), result.getInt("userrightID"));
        u.setId(result.getInt("id"));
        return u;
    }

    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET firebrigadeID=?, username=?, password=?, email=?, "
                + "userrightID=? WHERE id=?";   
        return sql;
    }

    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (firebrigadeID, username, password, email, userrightID) " + "VALUES (?,?,?,?,?)";
        return sql;
    }

    @Override
    protected void mapPojoToStatementUpdate(User t, PreparedStatement stmt) throws SQLException {       
        stmt.setInt(1, t.getFirebrigadeID());
        stmt.setString(2, t.getUsername());
        stmt.setInt(3, t.getPassword());
        stmt.setString(4, t.getEmail());
        stmt.setInt(5, t.getUserrightID());
        stmt.setInt(6, t.getId());
    }

    @Override
    protected void mapPojoToStatementInsert(User t, PreparedStatement stmt) throws SQLException {
        stmt.setInt(1, t.getFirebrigadeID());
        stmt.setString(2, t.getUsername());
        stmt.setInt(3, t.getPassword());
        stmt.setString(4, t.getEmail());
        stmt.setInt(5, t.getUserrightID());
    }
    
}
