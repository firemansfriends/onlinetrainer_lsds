/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JDBCDAO;

import DAO.UserrightDAO;
import POJO.Userright;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Lisa
 */
public class UserrightJDBCDAO extends JDBCBaseDAO<Userright> implements UserrightDAO{

    public UserrightJDBCDAO(){
        super("userright");
    }
    
    @Override
    protected Userright getPojoFromResultSet(ResultSet result) throws SQLException {
        Userright u = new Userright(result.getString("userright"));
        u.setId(result.getInt("id"));
        return u;
    }

    @Override
    protected String getUpdateString() throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET userright=? WHERE id=?";   
        return sql;
    }

    @Override
    protected String getInsertString() throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (userright) " + "VALUES (?)";
        return sql;
    }

    @Override
    protected void mapPojoToStatementUpdate(Userright t, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, t.getUserright());
        stmt.setInt(2, t.getId());
    }

    @Override
    protected void mapPojoToStatementInsert(Userright t, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, t.getUserright());
    }
    
}
