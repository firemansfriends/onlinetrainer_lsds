/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package POJO;

import java.util.Objects;

/**
 *
 * @author Lisa
 */
public class Answer implements Identifiable{
    private int id;
    private int fbNR;
    private String text;
    private int questionID;
    private boolean correct;
    private String backcolor="background:white";
    private String textcolor="color:black";
    private int numbering;
    private String picSelectionColor;
    private String picSelectionText;
    
    public Answer(){}
    public Answer(int fbNr,String text, int questionID, boolean correct, int numbering) {
        this.fbNR=fbNr;
        this.text = text;
        this.questionID = questionID;
        this.correct = correct;
        this.numbering=numbering;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public boolean getCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public int getFbNR() {
        return fbNR;
    }

    public void setFbNR(int fbNR) {
        this.fbNR = fbNR;
    }

    @Override
    public String toString() {
        return text;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        hash = 97 * hash + this.fbNR;
        hash = 97 * hash + Objects.hashCode(this.text);
        hash = 97 * hash + this.questionID;
        hash = 97 * hash + (this.correct ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Answer other = (Answer) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.fbNR != other.fbNR) {
            return false;
        }
        if (this.questionID != other.questionID) {
            return false;
        }
        if (this.correct != other.correct) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        return true;
    }
    public String getBackcolor(){
            return backcolor;
    }

    public void setBackcolor(String backcolor) {
        this.backcolor = backcolor;
    }

    public String getTextcolor() {
        return textcolor;
    }

    public void setTextcolor(String textcolor) {
        this.textcolor = textcolor;
    }

    public String getPicSelectionColor() {
        return picSelectionColor;
    }

    public void setPicSelectionColor(String picSelectionColor) {
        this.picSelectionColor = picSelectionColor;
    }

    public String getPicSelectionText() {
        return picSelectionText;
    }

    public void setPicSelectionText(String picSelectionText) {
        this.picSelectionText = picSelectionText;
    }

    public int getNumbering() {
        return numbering;
    }

    public void setNumbering(int numbering) {
        this.numbering = numbering;
    }
    
}
