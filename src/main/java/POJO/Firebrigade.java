/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package POJO;

import java.util.Objects;

/**
 *
 * @author Denise
 */
public class Firebrigade implements Identifiable{
    private int id;
    private int firebrigadenumber;
    private String location;
    private String commander;
    private String youthCommander;
    private String foundingYear;
    private String rescueEquipment;

    public Firebrigade(int firebrigadenumber, String location, String commander, String youthCommander, String foundingYear, String rescueEquipment) {
        this.firebrigadenumber = firebrigadenumber;
        this.location = location;
        this.commander = commander;
        this.youthCommander = youthCommander;
        this.foundingYear = foundingYear;
        this.rescueEquipment = rescueEquipment;
    }

    public Firebrigade() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFirebrigadenumber() {
        return firebrigadenumber;
    }

    public void setFirebrigadenumber(int firebrigadenumber) {
        this.firebrigadenumber = firebrigadenumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCommander() {
        return commander;
    }

    public void setCommander(String commander) {
        this.commander = commander;
    }

    public String getYouthCommander() {
        return youthCommander;
    }

    public void setYouthCommander(String youthCommander) {
        this.youthCommander = youthCommander;
    }

    public String getFoundingYear() {
        return foundingYear;
    }

    public void setFoundingYear(String foundingYear) {
        this.foundingYear = foundingYear;
    }

    public String getRescueEquipment() {
        return rescueEquipment;
    }

    public void setRescueEquipment(String rescueEquipment) {
        this.rescueEquipment = rescueEquipment;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + this.id;
        hash = 23 * hash + this.firebrigadenumber;
        hash = 23 * hash + Objects.hashCode(this.location);
        hash = 23 * hash + Objects.hashCode(this.commander);
        hash = 23 * hash + Objects.hashCode(this.youthCommander);
        hash = 23 * hash + Objects.hashCode(this.foundingYear);
        hash = 23 * hash + Objects.hashCode(this.rescueEquipment);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj){
        final Firebrigade other = (Firebrigade) obj;
        if (!Objects.equals(this.location, other.location)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return Integer.toString(id)+"-"+this.location;
    }
}
