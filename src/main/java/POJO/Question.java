/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package POJO;

import java.util.Objects;

/**
 *
 * @author Lisa
 */
public class Question implements Identifiable{
    private int id;
    private String text;
    private int categoryID;
    private int questionTypeID;
    private String hint;

    public Question(String text, int categoryID, int questionTypeID, String hint) {
        this.text = text;
        this.categoryID = categoryID;
        this.questionTypeID = questionTypeID;
        this.hint=hint;
    }
    public Question(){
        this.id=0;
        this.text = "";
        this.categoryID = 0;
        this.questionTypeID = 0;
        this.hint="";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getQuestionTypeID() {
        return questionTypeID;
    }

    public void setQuestionTypeID(int questionTypeID) {
        this.questionTypeID = questionTypeID;
    }

    @Override
    public String toString() {
        return "Question{" + "id=" + id + ", text=" + text + ", categoryID=" + categoryID + ", questionTypeID=" + questionTypeID + ", hint=" + hint + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.id;
        hash = 59 * hash + Objects.hashCode(this.text);
        hash = 59 * hash + this.categoryID;
        hash = 59 * hash + this.questionTypeID;
        hash = 59 * hash + Objects.hashCode(this.hint);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Question other = (Question) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.categoryID != other.categoryID) {
            return false;
        }
        if (this.questionTypeID != other.questionTypeID) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.hint, other.hint)) {
            return false;
        }
        return true;
    }
    
    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }
    
}
