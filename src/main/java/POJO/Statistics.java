/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package POJO;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Lisa
 */
public class Statistics implements Identifiable{
    
    private int id;
    private int firebrigadeID;
    private int counter;
    private LocalDate actualDate;
    
    public Statistics(){
        
    }

    public Statistics(int firebrigadeID, int counter, LocalDate actualDate) {
        this.firebrigadeID = firebrigadeID;
        this.counter = counter;
        this.actualDate = actualDate;
    }
    
    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public int getFirebrigadeID() {
        return firebrigadeID;
    }

    public void setFirebrigadeID(int firebrigadeID) {
        this.firebrigadeID = firebrigadeID;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public LocalDate getActualDate() {
        return actualDate;
    }

    public void setActualDate(LocalDate actualDate) {
        this.actualDate = actualDate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.id;
        hash = 73 * hash + this.firebrigadeID;
        hash = 73 * hash + this.counter;
        hash = 73 * hash + Objects.hashCode(this.actualDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Statistics other = (Statistics) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.firebrigadeID != other.firebrigadeID) {
            return false;
        }
        if (this.counter != other.counter) {
            return false;
        }
        if (!Objects.equals(this.actualDate, other.actualDate)) {
            return false;
        }
        return true;
    }
}
