/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package POJO;

/**
 *
 * @author Lisa
 */
public class TestStep_Category implements Identifiable{
    private int id;
    private int testStepID;
    private int categoryID;

    public TestStep_Category(int testStepID,int categoryID) {
        this.categoryID = categoryID;
        this.testStepID = testStepID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getTestStepID() {
        return testStepID;
    }

    public void setTestStepID(int testStepID) {
        this.testStepID = testStepID;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        hash = 79 * hash + this.categoryID;
        hash = 79 * hash + this.testStepID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TestStep_Category other = (TestStep_Category) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.categoryID != other.categoryID) {
            return false;
        }
        if (this.testStepID != other.testStepID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TestStep_Category{" + "id=" + id + ", testStepID=" + testStepID + ", categoryID=" + categoryID + '}';
    }

}
