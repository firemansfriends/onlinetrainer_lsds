/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package POJO;

/**
 *
 * @author Lisa
 */
public class TestStep_Question implements Identifiable{
    
    private int id;
    private int teststepID;
    private int questionID;

    public TestStep_Question(int teststepID, int questionID) {
        this.teststepID = teststepID;
        this.questionID = questionID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTeststepID() {
        return teststepID;
    }

    public void setTeststepID(int teststepID) {
        this.teststepID = teststepID;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    @Override
    public String toString() {
        return "Teststep_Question{" + "id=" + id + ", teststepID=" + teststepID + ", questionID=" + questionID + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.id;
        hash = 73 * hash + this.teststepID;
        hash = 73 * hash + this.questionID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TestStep_Question other = (TestStep_Question) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.teststepID != other.teststepID) {
            return false;
        }
        if (this.questionID != other.questionID) {
            return false;
        }
        return true;
    }
    
}
