/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package POJO;

import java.util.Objects;

/**
 *
 * @author Lisa
 */
public class User implements Identifiable{
    
    private int id;
    private int firebrigadeID;
    private String username;
    private int password;
    private String email;
    private int userrightID;

    public User(int firebrigadeID, String username, int password, String email, int userrightID) {
        this.firebrigadeID = firebrigadeID;
        this.username = username;
        this.password = password;
        this.email = email;
        this.userrightID = userrightID;
    }
    
    public User(){
        
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public int getFirebrigadeID() {
        return firebrigadeID;
    }

    public void setFirebrigadeID(int firebrigadeID) {
        this.firebrigadeID = firebrigadeID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUserrightID() {
        return userrightID;
    }

    public void setUserrightID(int userrightID) {
        this.userrightID = userrightID;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", firebrigadeID=" + firebrigadeID + ", username=" + username + ", password=" + password + ", email=" + email + ", userrightID=" + userrightID + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.id;
        hash = 37 * hash + this.firebrigadeID;
        hash = 37 * hash + Objects.hashCode(this.username);
        hash = 37 * hash + this.password;
        hash = 37 * hash + Objects.hashCode(this.email);
        hash = 37 * hash + this.userrightID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.firebrigadeID != other.firebrigadeID) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (this.password != other.password) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (this.userrightID != other.userrightID) {
            return false;
        }
        return true;
    }
    
    
}
