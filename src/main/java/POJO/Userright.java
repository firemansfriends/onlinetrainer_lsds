/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package POJO;
import java.util.Objects;

/**
 *
 * @author Lisa
 */
public class Userright implements Identifiable{
    private int id;
    private String userright;

    public Userright(String userright) {
        this.userright = userright;
    }
    
    public Userright(){
        
    }
    
    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getUserright() {
        return userright;
    }

    public void setUserright(String userright) {
        this.userright = userright;
    }

    @Override
    public String toString() {
        return Integer.toString(id)+"-"+this.userright;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.userright);
        return hash;
    }

    /*@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Userright other = (Userright) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.userright, other.userright)) {
            return false;
        }
        return true;
    }*/
    
    @Override
    public boolean equals(Object obj) {
        final Userright other = (Userright) obj;
        if (!Objects.equals(this.userright, other.userright)) {
            return false;
        }
        return true;
    }
}
