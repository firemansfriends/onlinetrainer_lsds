/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Properties;

import java.io.FileReader;
import java.util.Properties;
import javax.faces.context.FacesContext;


public class PathProperties {
    private static final PathProperties INSTANCE = new PathProperties();
    public final static String IMAGEPATH="imagePath";
    private Properties p;
    
    private PathProperties(){        
        try{
            p=new Properties();    
            String realpath=FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
            p.load(new FileReader(realpath+"paths.properties"));           
        }catch(Exception e){
            e.printStackTrace();
        }  
    }
    public String getStringProperty(String propertyName){
        return p.getProperty(propertyName);
    }
    public static PathProperties getInstance() {
        return INSTANCE;
    }

}
