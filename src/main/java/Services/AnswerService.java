package Services;

import DAO.AnswerDAO;
import JDBCDAO.AnswerJDBCDAO;
import POJO.Answer;
import java.util.ArrayList;
import java.util.List;

 
public class AnswerService implements Service<Answer>{
    private AnswerDAO answerDAO;
    private List<Answer> answerList;
    private static final AnswerService instance=new AnswerService();

    public static AnswerService getInstance(){
        return instance;
    }
    private AnswerService() {
        answerDAO = new AnswerJDBCDAO();
        answerList = answerDAO.list();
    }

    @Override
    public boolean insertObject(Answer object) {
        answerDAO.create(object);
        answerList.add(object);
        return true;
    }

    @Override
    public void updateObject(Answer object) {
        answerDAO.update(object);
        answerList = answerDAO.list();
    }

    @Override
    public boolean deleteObject(Answer object) {
        answerDAO.delete(object);
        answerList.remove(object);
        return true;
    }
    @Override
    public List<Answer> getList() {
        return answerList;
    }

    @Override
    public Answer readObject(int id) {
        return answerDAO.read(id);
    }
    public List<Answer> getAnswersPerQuestion(int quId){
        List<Answer> aList=new ArrayList<>();
        for(int idx=0;idx<getList().size();idx++){
            if(getList().get(idx).getQuestionID()==quId){
                aList.add(getList().get(idx));
            }
        }
        return aList;
    }
}
