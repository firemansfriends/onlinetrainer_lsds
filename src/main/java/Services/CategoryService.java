/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import DAO.CategoryDAO;
import JDBCDAO.CategoryJDBCDAO;
import POJO.Category;
import java.util.List;

/**
 *
 * @author Lisa
 */
public class CategoryService implements Service<Category> {

    private CategoryDAO categoryDAO;
    private List<Category> categoryList;
    private static final CategoryService instance = new CategoryService();


    public static CategoryService getInstance() {
        return instance;
    }

    private CategoryService() {
        categoryDAO = new CategoryJDBCDAO();
        categoryList = categoryDAO.list();
    }

    @Override
    public boolean insertObject(Category object) {
        categoryDAO.create(object);
        categoryList.add(object);
        return true;
    }

    @Override
    public void updateObject(Category object) {
        categoryDAO.update(object);
        categoryList = categoryDAO.list();
    }

    @Override
    public boolean deleteObject(Category object) {
        if(categoryDAO.delete(object)){
            categoryList.remove(object);
            return true;
        }
        return false;
    }

    @Override
    public List<Category> getList() {
        return categoryList;
    }

    @Override
    public Category readObject(int id) {
        return categoryDAO.read(id);
    }  
}
