package Services;

import DAO.FirebrigadeDAO;
import JDBCDAO.FirebrigadeJDBCDAO;
import POJO.Firebrigade;
import java.util.List;

 
public class FirebrigadeService implements Service<Firebrigade>{
    private FirebrigadeDAO firebrigadeDAO;
    private List<Firebrigade> firebrigadeList;
    private static final FirebrigadeService instance=new FirebrigadeService();

    public static FirebrigadeService getInstance(){
        return instance;
    }
    private FirebrigadeService() {
        firebrigadeDAO = new FirebrigadeJDBCDAO();
        firebrigadeList = firebrigadeDAO.list();
    }

    @Override
    public boolean insertObject(Firebrigade object) {
        firebrigadeDAO.create(object);
        firebrigadeList.add(object);
        return true;
    }

    @Override
    public void updateObject(Firebrigade object) {
        firebrigadeDAO.update(object);
        firebrigadeList = firebrigadeDAO.list();
    }

    @Override
    public boolean deleteObject(Firebrigade object) {
        firebrigadeDAO.delete(object);
        firebrigadeList.remove(object);
        return true;
    }
    @Override
    public List<Firebrigade> getList() {
        return firebrigadeList;
    }

    @Override
    public Firebrigade readObject(int id) {
        return firebrigadeDAO.read(id);
    }
    
    public String getLocationToFirebrigadeID(int id){
        String ret="";
        
        for(int idx=0; idx<firebrigadeList.size(); idx++){
            if(id==firebrigadeList.get(idx).getId()){
                ret = firebrigadeList.get(idx).getLocation();
            }
        }
        return ret;
    }
    public Firebrigade getFirebrigadeToNumber(int fbNum){
        for(Firebrigade fb: firebrigadeList){
            if(fb.getFirebrigadenumber()==fbNum){
                return fb;
            }
        }
        return null;
    }
}
