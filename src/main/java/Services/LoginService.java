/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import POJO.Firebrigade;
import POJO.Statistics;
import POJO.User;

/**
 *
 * @author Lisa
 */
public class LoginService {

    private String name;  // name which is entered when a normal user logs in
    private Firebrigade firebrigade = new Firebrigade(); // firebrigade to which the normal user belongs to
    private String username; // username of administrator or superuser
    private String password; // password of administrator or superuser
    private String nameOfUser; // is shown on the home page
    private int userrightID; //id of the userright
    private int id; // id of user
    private String email;
    private boolean isMailChanged = false;
    private String oldPassword = null; // password entered when changing it in the personal data
    private String newPassword = null;
    private String newPasswordConfirm = null;
    
    private User currentUser;
    private Firebrigade currentfirebrigade = new Firebrigade();
    private Statistics currentStatistics;

    private boolean isAdmin = false;
    private boolean isAdminOrSuperUser = false;
    private boolean isSuperUser = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Firebrigade getFirebrigade() {
        return firebrigade;
    }

    public void setFirebrigade(Firebrigade firebrigade) {
        this.firebrigade = firebrigade;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNameOfUser() {
        return nameOfUser;
    }

    public void setNameOfUser(String nameOfUser) {
        this.nameOfUser = nameOfUser;
    }

    public int getUserrightID() {
        return userrightID;
    }

    public void setUserrightID(int userrightID) {
        this.userrightID = userrightID;
    }

    public boolean isIsAdmin() {
        if (userrightID == 1) {
            this.isAdmin = true;
        } else {
            this.isAdmin = false;
        }
        return this.isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public boolean isIsAdminOrSuperUser() {
        if (this.userrightID == 1 || this.userrightID == 2) {
            this.isAdminOrSuperUser = true;
        } else {
            this.isAdminOrSuperUser = false;
        }
        return this.isAdminOrSuperUser;
    }
    
    public void setIsAdminOrSuperUser(boolean isAdminOrSuperUser) {
        this.isAdminOrSuperUser = isAdminOrSuperUser;
    }

    public boolean isIsSuperUser() {
        if (this.userrightID == 2) {
            this.isSuperUser = true;
        } else {
            this.isSuperUser = false;
        }
        return this.isSuperUser;
    }
    
    public void setIsSuperUser(boolean isSuperUser) {
        this.isSuperUser = isSuperUser;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public Firebrigade getCurrentfirebrigade() {
        return currentfirebrigade;
    }

    public void setCurrentfirebrigade(Firebrigade currentfirebrigade) {
        this.currentfirebrigade = currentfirebrigade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isIsMailChanged() {
        return isMailChanged;
    }

    public void setIsMailChanged() {
        this.isMailChanged = true;
    }
    
    public void setIsMailChanged(boolean i){
        this.isMailChanged = i;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordConfirm() {
        return newPasswordConfirm;
    }

    public void setNewPasswordConfirm(String newPasswordConfirm) {
        this.newPasswordConfirm = newPasswordConfirm;
    }

    public Statistics getCurrentStatistics() {
        return currentStatistics;
    }

    public void setCurrentStatistics(Statistics currentStatistics) {
        this.currentStatistics = currentStatistics;
    }

}
