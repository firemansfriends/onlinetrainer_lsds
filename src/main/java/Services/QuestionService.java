package Services;

import DAO.QuestionDAO;
import JDBCDAO.QuestionJDBCDAO;
import POJO.Question;
import java.util.List;

public class QuestionService implements Service<Question> {

    private QuestionDAO questionDAO;
    private List<Question> questionList;
    
    
    private static final QuestionService instance = new QuestionService();

    public static QuestionService getInstance() {
        return instance;
    }

    private QuestionService() {
        questionDAO = new QuestionJDBCDAO();
        questionList = questionDAO.list();       
    }

    @Override
    public boolean insertObject(Question object) {
        if (questionDAO.create(object)) {
            questionList.add(object);
            return true;
        }
        return false;
    }

    @Override
    public void updateObject(Question object) {
        questionDAO.update(object);
        questionList = questionDAO.list();
    }

    @Override
    public boolean deleteObject(Question object) {
        questionDAO.delete(object);
        questionList.remove(object);
        return true;
    }

    @Override
    public List<Question> getList() {
        return questionList;
    }

    @Override
    public Question readObject(int id) {
        return questionDAO.read(id);
    }
}
