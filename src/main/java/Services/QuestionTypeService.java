package Services;

import DAO.QuestionTypeDAO;
import JDBCDAO.QuestionTypeJDBCDAO;
import POJO.QuestionType;
import java.util.List;

public class QuestionTypeService implements Service<QuestionType>{
    private QuestionTypeDAO questiontypeDAO;
    private List<QuestionType> questiontypeList;
    private static final QuestionTypeService instance=new QuestionTypeService();

    public static QuestionTypeService getInstance(){
        return instance;
    }
    private QuestionTypeService() {
        questiontypeDAO = new QuestionTypeJDBCDAO();
        questiontypeList = questiontypeDAO.list();
    }

    @Override
    public boolean insertObject(QuestionType object) {
        questiontypeDAO.create(object);
        questiontypeList.add(object);
        return true;
    }

    @Override
    public void updateObject(QuestionType object) {
        questiontypeDAO.update(object);
        questiontypeList = questiontypeDAO.list();
    }

    @Override
    public boolean deleteObject(QuestionType object) {
        questiontypeDAO.delete(object);
        questiontypeList.remove(object);
        return true;
    }
    @Override
    public List<QuestionType> getList() {
        return questiontypeList;
    }
    @Override
    public QuestionType readObject(int id) {
        return questiontypeDAO.read(id);
    }
}
