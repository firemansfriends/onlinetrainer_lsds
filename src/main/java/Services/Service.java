/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Services;

import java.util.List;

/**
 *
 * @author Lisa
 */
public interface Service<T> {
    public boolean insertObject(T object);
    public void updateObject(T object);
    public boolean deleteObject(T object);
    public List<T> getList();
    public T readObject(int id);
}
