/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import DAO.StatisticsDAO;
import JDBCDAO.StatisticsJDBCDAO;
import POJO.Statistics;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lisa
 */
public class StatisticsService implements Service<Statistics> {

    private StatisticsDAO statisticsDAO;
    private List<Statistics> statisticsList;
    private static final StatisticsService instance = new StatisticsService();

    public static StatisticsService getInstance() {
        return instance;
    }

    private StatisticsService() {
        statisticsDAO = new StatisticsJDBCDAO();
        statisticsList = statisticsDAO.list();
    }

    @Override
    public boolean insertObject(Statistics object) {
        statisticsDAO.create(object);
        statisticsList.add(object);
        return true;
    }

    @Override
    public void updateObject(Statistics object) {
        statisticsDAO.update(object);
        statisticsList = statisticsDAO.list();
    }

    @Override
    public boolean deleteObject(Statistics object) {
        statisticsDAO.delete(object);
        statisticsList.remove(object);
        return true;
    }

    @Override
    public List<Statistics> getList() {
        return statisticsList;
    }

    @Override
    public Statistics readObject(int id) {
        return statisticsDAO.read(id);
    }

    public ArrayList<Integer> getIDsToFirebrigadeID(int fid) {
        ArrayList<Integer> ids = new ArrayList<>();

        for (int idx = 0; idx < statisticsList.size(); idx++) {
            if (fid == statisticsList.get(idx).getFirebrigadeID()) {
                ids.add(statisticsList.get(idx).getId());
            }
        }
        return ids;
    }

    public ArrayList<Statistics> getStatisticsPerMonth(int month, int year) {
        ArrayList<Statistics> stList = new ArrayList<>();

        for (int idx = 0; idx < statisticsList.size(); idx++) {
            if (statisticsList.get(idx).getActualDate().getMonthValue() == month
                    && statisticsList.get(idx).getActualDate().getYear() == year) {
                stList.add(statisticsList.get(idx));
            }
        }

        return stList;
    }
    
    public ArrayList<Integer> getAvailableYears(){
        ArrayList<Integer> yearList = new ArrayList<>();
        
        for(int idx=0; idx < statisticsList.size(); idx++){
            if(!yearList.contains(statisticsList.get(idx).getActualDate().getYear())){
                yearList.add(statisticsList.get(idx).getActualDate().getYear());
            }    
        }
        
        return yearList;
    }
}
