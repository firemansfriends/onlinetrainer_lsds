/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Services;

import DAO.TestDAO;
import JDBCDAO.TestJDBCDAO;
import POJO.Test;
import java.util.List;

/**
 *
 * @author Lisa
 */
public class TestService implements Service<Test>{
    private TestDAO testDAO;
    private List<Test> testList;
    private static final TestService instance=new TestService();
 
    public static TestService getInstance(){
        return instance;
    }
    
    private TestService() {
        testDAO = new TestJDBCDAO();
        testList = testDAO.list();
    }

    @Override
    public boolean insertObject(Test object) {
        testDAO.create(object);
        testList.add(object);
        return true;
    }

    @Override
    public void updateObject(Test object) {
        testDAO.update(object);
        testList=testDAO.list();
    }

    @Override
    public boolean deleteObject(Test object) {
        if(testDAO.delete(object)){
            testList.remove(object);
            return true;
        }
        return false;
    }            
    @Override
    public List<Test> getList() {
        return testList;
    }

    @Override
    public Test readObject(int id) {
        return testDAO.read(id);
    }
}
