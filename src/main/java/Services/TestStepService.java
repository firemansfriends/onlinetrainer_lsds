/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import DAO.TestStepDAO;
import JDBCDAO.TestStepJDBCDAO;
import POJO.TestStep;
import java.util.List;

/**
 *
 * @author Lisa
 */
public class TestStepService implements Service<TestStep> {

    private TestStepDAO teststepDAO;
    private List<TestStep> teststepList;
 
    private static final TestStepService instance = new TestStepService();

    public static TestStepService getInstance() {
        return instance;
    }

    private TestStepService() {
        teststepDAO = new TestStepJDBCDAO();
        teststepList = teststepDAO.list();
    }

    @Override
    public boolean insertObject(TestStep object) {
        teststepDAO.create(object);
        teststepList.add(object);
        return true;
    }

    @Override
    public void updateObject(TestStep object) {
        teststepDAO.update(object);
        teststepList = teststepDAO.list();
    }

    @Override
    public boolean deleteObject(TestStep object) {
        if(teststepDAO.delete(object)){
            teststepList.remove(object);
            return true;
        }       
        return false;
    }

    @Override
    public List<TestStep> getList() {
        return teststepList;
    }

    @Override
    public TestStep readObject(int id) {
        return teststepDAO.read(id);
    }
 }
