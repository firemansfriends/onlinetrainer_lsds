/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Services;

import DAO.TestStep_CategoryDAO;
import JDBCDAO.TestStep_CategoryJDBCDAO;
import POJO.TestStep_Category;
import java.util.List;

/**
 *
 * @author Lisa
 */
public class TestStep_CategoryService implements Service<TestStep_Category>{
    private TestStep_CategoryDAO t_cDAO;
    private List<TestStep_Category> t_cList;
    private static final TestStep_CategoryService instance=new TestStep_CategoryService();
   
    public static TestStep_CategoryService getInstance(){
        return instance;
    }
    
    private TestStep_CategoryService() {
        t_cDAO = new TestStep_CategoryJDBCDAO();
        t_cList = t_cDAO.list();
    }

    @Override
    public boolean insertObject(TestStep_Category object) {
        t_cDAO.create(object);
        t_cList.add(object);
        return true;
    }

    @Override
    public void updateObject(TestStep_Category object) {
        t_cDAO.update(object);
        t_cList=t_cDAO.list();
    }

    @Override
    public boolean deleteObject(TestStep_Category object) {
        t_cDAO.delete(object);
        t_cList.remove(object);
        return true;
    }
    @Override
    public List<TestStep_Category> getList() {
        return t_cList;
    }

    @Override
    public TestStep_Category readObject(int id) {
        return t_cDAO.read(id);
    }
}
