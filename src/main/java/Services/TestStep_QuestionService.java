/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Services;

import DAO.TestStep_QuestionDAO;
import JDBCDAO.TestStep_QuestionJDBCDAO;
import POJO.TestStep_Question;
import java.util.List;

/**
 *
 * @author Lisa
 */
public class TestStep_QuestionService implements Service<TestStep_Question>{
    private TestStep_QuestionDAO t_qDAO;
    private List<TestStep_Question> t_qList;    
    private static final TestStep_QuestionService instance=new TestStep_QuestionService();
   
    public static TestStep_QuestionService getInstance(){
        return instance;
    }
    
    private TestStep_QuestionService() {
        t_qDAO = new TestStep_QuestionJDBCDAO();
        t_qList = t_qDAO.list();
    }

    @Override
    public boolean insertObject(TestStep_Question object) {
        t_qDAO.create(object);
        t_qList.add(object);
        return true;
    }

    @Override
    public void updateObject(TestStep_Question object) {
        t_qDAO.update(object);
        t_qList=t_qDAO.list();
    }

    @Override
    public boolean deleteObject(TestStep_Question object) {
        t_qDAO.delete(object);
        t_qList.remove(object);
        return true;
    }
    @Override
    public List<TestStep_Question> getList() {
        return t_qList;
    }

    @Override
    public TestStep_Question readObject(int id) {
        return t_qDAO.read(id);
    }
    public void deleteStepQuestion(int sID, int qID){
        for(TestStep_Question tq:t_qList){
            if(tq.getTeststepID()==sID&&tq.getQuestionID()==qID){
                deleteObject(tq);
                return;
            }
        }
    }
}
