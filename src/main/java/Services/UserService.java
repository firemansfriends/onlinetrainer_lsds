/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Services;

import DAO.UserDAO;
import JDBCDAO.UserJDBCDAO;
import POJO.User;
import java.util.List;

/**
 *
 * @author Lisa
 */
public class UserService implements Service<User>{
    
    private UserDAO userDAO;
    private List<User> userList;
    private static final UserService instance=new UserService();
 
    public static UserService getInstance(){
        return instance;
    }
    
    private UserService() {
        userDAO = new UserJDBCDAO();
        userList = userDAO.list();
    }

    @Override
    public boolean insertObject(User object) {
        userDAO.create(object);
        userList.add(object);
        return true;
    }

    @Override
    public void updateObject(User object) {
        userDAO.update(object);
        userList=userDAO.list();
    }

    @Override
    public boolean deleteObject(User object) {
        userDAO.delete(object);
        userList.remove(object);
        return true;
    }

    @Override
    public List<User> getList() {
        return userList;
    }

    @Override
    public User readObject(int id) {
        return userDAO.read(id);
    }
    
}
