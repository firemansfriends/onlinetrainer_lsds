/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Services;

import DAO.UserrightDAO;
import JDBCDAO.UserrightJDBCDAO;
import POJO.Userright;
import java.util.List;

/**
 *
 * @author Lisa
 */
public class UserrightService implements Service<Userright>{
    
    private UserrightDAO userrightDAO;
    private List<Userright> userrightList;
    private static final UserrightService instance=new UserrightService();

    public static UserrightService getInstance(){
        return instance;
    }
    
    private UserrightService() {
        userrightDAO = new UserrightJDBCDAO();
        userrightList = userrightDAO.list();
    }
    
    @Override
    public boolean insertObject(Userright object) {
        userrightDAO.create(object);
        userrightList.add(object);
        return true;}

    @Override
    public void updateObject(Userright object) {
        userrightDAO.update(object);
        userrightList=userrightDAO.list();
    }

    @Override
    public boolean deleteObject(Userright object) {
        userrightDAO.delete(object);
        userrightList.remove(object);
        return true;}

    @Override
    public List<Userright> getList() {
        return userrightList;
    }

    @Override
    public Userright readObject(int id) {
        return userrightDAO.read(id);
    }   
    
    public String getUserrightToID(int id){
        String ret="";
        
        for(int idx=0; idx<userrightList.size(); idx++){
            if(id==userrightList.get(idx).getId()){
                ret = userrightList.get(idx).getUserright();
            }
        }
        return ret;
    }
}
